<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Context;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class WithSubContext extends AbstractParser
{
    public readonly ParserInterface $parser;

    public function __construct(
        protected readonly mixed $subContext,
        ParserInterface|array|string $parser,
    ) {
        $this->parser = AbstractParser::from($parser);
    }

    public function parse(array|string|StreamInterface $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);
        $initialContext = $stream->getContext();
        $streamWithSubContext = $stream->withContext($this->subContext);

        return $this->parser->parse($streamWithSubContext)->withContext($initialContext);
    }
}
