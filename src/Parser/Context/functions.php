<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Context;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;

function withSubContext(mixed $subContext, ParserInterface|array|string $parser): WithSubContext
{
    return new WithSubContext($subContext, $parser);
}

/**
 * @param callable(ParseResult): mixed $contextProvider
 */
function alterContext(ParserInterface|array|string $parser, callable $contextProvider): AlterContext
{
    return new AlterContext($parser, $contextProvider);
}
