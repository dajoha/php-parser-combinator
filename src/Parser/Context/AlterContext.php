<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Context;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class AlterContext extends AbstractParser
{
    public readonly ParserInterface $parser;

    /** @var callable(ParseResult): mixed */
    public readonly mixed $contextProvider;

    /**
     * @param callable(ParseResult): mixed $contextProvider
     */
    public function __construct(ParserInterface|array|string $parser, callable $contextProvider)
    {
        $this->parser = AbstractParser::from($parser);
        $this->contextProvider = $contextProvider;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $parseResult = $this->parser->parse($input);

        if ($parseResult->isSuccess()) {
            $newContext = ($this->contextProvider)($parseResult);
            $parseResult = $parseResult->withContext($newContext);
        }

        return $parseResult;
    }

    public function getDescription(): string
    {
        return "{$this->parser->getDescription()} with altered context";
    }
}
