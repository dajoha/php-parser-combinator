<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Combinator;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class OrFatal extends AbstractParser
{
    public readonly ParserInterface $parser;

    public function __construct(
        ParserInterface|array|string $parser,
        public readonly ?string $fatalContextMessage = null,
    ) {
        $this->parser = AbstractParser::from($parser);
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $parseResult = $this->parser->parse($input);

        return $parseResult->isError()
            ? ParseResult::fatal($parseResult->stream, $this->fatalContextMessage)
            : $parseResult;
    }

    public function getDescription(): string
    {
        return "A match of {$this->parser->getDescriptionLc()}, or fatal";
    }
}
