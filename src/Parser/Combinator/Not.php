<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Combinator;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Not extends AbstractParser
{
    public readonly ParserInterface $parser;

    public function __construct(ParserInterface|array|string $parser)
    {
        $this->parser = AbstractParser::from($parser);
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $parseResult = $this->parser->parse($stream);

        return match ($parseResult->status) {
            ParseStatus::Success => ParseResult::error($parseResult->stream),
            ParseStatus::Error => ParseResult::success($stream, null),
            ParseStatus::Fatal => $parseResult,
        };
    }
    public function getDescription(): string
    {
        return "Not {$this->parser->getDescriptionLc()}";
    }
}
