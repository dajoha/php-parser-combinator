<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Combinator;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Verify extends AbstractParser
{
    public readonly ParserInterface $parser;

    /** @var callable(mixed, ParseResult): bool $callback */
    public readonly mixed $callback;

    /**
     * @param callable(mixed $token, ParseResult): bool $callback
     */
    public function __construct(ParserInterface|array|string $parser, callable $callback)
    {
        $this->parser = AbstractParser::from($parser);
        $this->callback = $callback;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $parseResult = $this->parser->parse($input);
        if (!$parseResult->isSuccess()) {
            return $parseResult;
        }

        return match (($this->callback)($parseResult->output, $parseResult)) {
            true => ParseResult::success($parseResult->stream, $parseResult->output),
            false => ParseResult::error($parseResult->stream),
        };
    }

    public function getDescription(): string
    {
        return "{$this->parser->getDescription()} which verifies a predicate";
    }
}
