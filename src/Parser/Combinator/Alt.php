<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Combinator;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Helper\DescriptionHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Alt extends AbstractParser
{
    /** @var ParserInterface[] */
    public readonly array $parsers;

    public function __construct(ParserInterface|array|string ...$parsers)
    {
        $this->parsers = array_map(fn($parser) => AbstractParser::from($parser), $parsers);
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        foreach ($this->parsers as $parser) {
            $parseResult = $parser->parse($stream);
            if (!$parseResult->isError()) {
                return $parseResult;
            }
        }

        return ParseResult::error($stream);
    }

    public function getDescription(): string
    {
        return DescriptionHelper::multiline($this->parsers, "A choice between", " or ");
    }
}
