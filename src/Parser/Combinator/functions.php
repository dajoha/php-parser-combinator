<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Combinator;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;

function alt(ParserInterface|array|string ...$parsers): Alt
{
    return new Alt(...$parsers);
}

function not(ParserInterface|array|string $parser): Not
{
    return new Not($parser);
}

function opt(ParserInterface|array|string $parser): Opt
{
    return new Opt($parser);
}

function orFatal(ParserInterface|array|string $parser): OrFatal
{
    return new OrFatal($parser);
}

/**
 * @param  callable(mixed, ParseResult): bool $callback
 */
function verify(ParserInterface|array|string $parser, callable $callback): Verify
{
    return new Verify($parser, $callback);
}
