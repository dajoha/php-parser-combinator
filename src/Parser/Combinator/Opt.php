<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Combinator;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Opt extends AbstractParser
{
    public readonly ParserInterface $parser;

    public function __construct(ParserInterface|array|string $parser)
    {
        $this->parser = AbstractParser::from($parser);
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $parseResult = $this->parser->parse($input);
        if ($parseResult->isFatal()) {
            return $parseResult;
        }

        $output = $parseResult->isSuccess() ? $parseResult->output : null;

        return ParseResult::success($parseResult->stream, $output);
    }

    public function getDescription(): string
    {
        return "An optional match of {$this->parser->getDescriptionLc()}";
    }
}
