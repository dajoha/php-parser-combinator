<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Sequence;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;

class Delimited extends InnerParser
{
    public readonly ParserInterface $left;
    public readonly ParserInterface $value;
    public readonly ParserInterface $right;

    public function __construct(
        ParserInterface|array|string $left,
        ParserInterface|array|string $value,
        ParserInterface|array|string $right,
    ) {
        $this->left = AbstractParser::from($left);
        $this->value = AbstractParser::from($value);
        $this->right = AbstractParser::from($right);

        $parser = seq($left, $value, $right)
            ->skipIgnored(false)
            ->map(fn($arr) => $arr[1])
        ;

        parent::__construct($parser);
    }

    public function getDescription(): string
    {
        $value = $this->value->getDescription();
        $left = $this->left->getDescriptionLc();
        $right = $this->right->getDescriptionLc();

        return "$value delimited by $left and $right";
    }
}
