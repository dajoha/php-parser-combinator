<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Sequence;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Helper\DescriptionHelper;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;

class Ignored extends InnerParser
{
    public readonly ParserInterface $initialParser;

    public function __construct(ParserInterface|array|string $parser)
    {
        $this->initialParser = AbstractParser::from($parser);

        $parser = AbstractParser::from($parser);
        parent::__construct($parser->map(fn($output) => new IgnoredOutput($output)));
    }

    public function getDescription(): string
    {
        return DescriptionHelper::ignored($this->initialParser->getDescription());
    }
}
