<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper;

readonly class IgnoredOutput
{
    public function __construct(public mixed $output)
    {
    }
}
