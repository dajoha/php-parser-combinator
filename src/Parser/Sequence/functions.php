<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Sequence;

use Dajoha\ParserCombinator\Base\ParserInterface;

function delimited(
    ParserInterface|array|string $left,
    ParserInterface|array|string $value,
    ParserInterface|array|string $right,
): Delimited
{
    return new Delimited($left, $value, $right);
}

function ignored(ParserInterface|array|string $parser): Ignored
{
    return new Ignored($parser);
}

function followed(ParserInterface|array|string $value, ParserInterface|array|string $right): Followed
{
    return new Followed($value, $right);
}

function preceded(ParserInterface|array|string $left, ParserInterface|array|string $value): Preceded
{
    return new Preceded($left, $value);
}

function seq(ParserInterface|array|string ...$parsers): Seq
{
    return new Seq(...$parsers);
}
