<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Sequence;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;

class Followed extends InnerParser
{
    public readonly ParserInterface $value;
    public readonly ParserInterface $right;

    public function __construct(
        ParserInterface|array|string $value,
        ParserInterface|array|string $right,
    ) {
        $this->value = AbstractParser::from($value);
        $this->right = AbstractParser::from($right);

        $parser = seq($value, $right)
            ->skipIgnored(false)
            ->map(fn($arr) => $arr[0])
        ;

        parent::__construct($parser);
    }

    public function getDescription(): string
    {
        $value = $this->value->getDescription();
        $right = $this->right->getDescriptionLc();

        return "$value followed by $right";
    }
}
