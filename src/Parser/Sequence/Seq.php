<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Sequence;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Helper\DescriptionHelper;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Seq extends AbstractParser
{
    /** @var ParserInterface[] */
    public readonly array $parsers;

    public bool $skipIgnored = true;

    public function __construct(ParserInterface|array|string ...$parsers)
    {
        $this->parsers = array_map(fn($parser) => AbstractParser::from($parser), $parsers);
    }

    public function skipIgnored(bool $skipIgnored): self
    {
        $this->skipIgnored = $skipIgnored;

        return $this;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $output = [];

        foreach ($this->parsers as $parser) {
            $parseResult = $parser->parse($stream);
            if (!$parseResult->isSuccess()) {
                return $parseResult;
            }
            $stream = $parseResult->stream;
            if (!$this->skipIgnored || !$parseResult->output instanceof IgnoredOutput) {
                $output[] = $parseResult->output;
            }
        }

        return ParseResult::success($stream, $output);
    }

    public function getDescription(): string
    {
        $prefix = function(ParserInterface $parser): string {
            $index = 0;
            foreach ($this->parsers as $loopParser) {
                $toIgnore = (
                    $loopParser instanceof Ignored
                    || $loopParser instanceof InnerParser && $loopParser->parser instanceof Ignored
                );
                if ($parser === $loopParser) {
                    if ($toIgnore) {
                        return " ~. ";
                    } else {
                        return str_pad("$index", 2, ' ', STR_PAD_LEFT).'. ';
                    }
                }
                if (!$toIgnore) {
                    $index++;
                }
            }

            return " ?. ";
        };

        return DescriptionHelper::multiline(
            $this->parsers,
            "A sequence of",
            " and ",
            $prefix,
        );
    }
}
