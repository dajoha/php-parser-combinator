<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Sequence;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;

class Preceded extends InnerParser
{
    public readonly ParserInterface $left;
    public readonly ParserInterface $value;

    public function __construct(
        ParserInterface|array|string $left,
        ParserInterface|array|string $value,
    ) {
        $this->left = AbstractParser::from($left);
        $this->value = AbstractParser::from($value);

        $parser = seq($left, $value)
            ->skipIgnored(false)
            ->map(fn($arr) => $arr[1])
        ;

        parent::__construct($parser);
    }

    public function getDescription(): string
    {
        $value = $this->value->getDescription();
        $left = $this->left->getDescriptionLc();

        return "$value preceded by $left";
    }
}
