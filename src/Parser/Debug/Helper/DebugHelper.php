<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Debug\Helper;

class DebugHelper
{
    public static function dump(mixed ...$values): void
    {
        if (function_exists('dump')) {
            /** @noinspection PhpUndefinedFunctionInspection */
            dump(...$values);
        } else {
            var_dump(...$values);
        }
    }
}
