<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Debug;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;

class DumpOutput extends InnerParser
{
    public function __construct(ParserInterface|array|string $parser)
    {
        parent::__construct(new Dump($parser, fn(ParseResult $r) => $r->output));
    }

    public function getDescription(): string
    {
        return "{$this->parser->getDescription()} (output dumped)";
    }
}
