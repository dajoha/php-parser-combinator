<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Debug;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

/**
 * @param callable(ParseResult, StreamInterface): void $callback
 */
function debug(ParserInterface|array|string $parser, callable $callback): Debug
{
    return new Debug($parser, $callback);
}
