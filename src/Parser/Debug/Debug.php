<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Debug;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Debug extends AbstractParser
{
    public readonly ParserInterface $parser;

    /** @var callable(ParseResult, StreamInterface): void */
    protected $callback;

    /**
     * @param callable(ParseResult, StreamInterface): void $callback
     */
    public function __construct(ParserInterface|array|string $parser, callable $callback)
    {
        $this->parser = AbstractParser::from($parser);
        $this->callback = $callback;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $parseResult = $this->parser->parse($stream);
        ($this->callback)($parseResult, $stream);

        return $parseResult;
    }

    public function getDescription(): string
    {
        return "{$this->parser->getDescription()} with debug callback";
    }
}
