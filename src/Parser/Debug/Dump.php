<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Debug;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Debug\Helper\DebugHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Dump extends AbstractParser
{
    public readonly ParserInterface $parser;

    /** @var (callable(ParseResult): mixed)|null */
    public readonly mixed $parseResultMapper;

    /**
     * @param ParserInterface|array|string $parser
     * @param (callable(ParseResult): mixed)|null $parseResultMapper
     */
    public function __construct(
        ParserInterface|array|string $parser,
        ?callable $parseResultMapper = null,
    ) {
        $this->parser = AbstractParser::from($parser);
        $this->parseResultMapper = $parseResultMapper;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $parseResult = $this->parser->parse($input);

        $dumpValue = $parseResult;
        if (is_callable($this->parseResultMapper)) {
            $dumpValue = ($this->parseResultMapper)($parseResult);
        }
        DebugHelper::dump($dumpValue);

        return $parseResult;
    }

    public function getDescription(): string
    {
        return "{$this->parser->getDescription()} (dumped)";
    }
}
