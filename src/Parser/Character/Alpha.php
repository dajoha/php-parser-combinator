<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Character;

use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Token\oneOf;

class Alpha extends InnerParser
{
    public const CHARS = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    ];

    public function __construct()
    {
        parent::__construct(oneOf(self::CHARS));
    }

    public function getDescription(): string
    {
        return "One alphabetic char";
    }
}
