<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Character;

use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Token\oneOf;

class Digit extends InnerParser
{
    public const DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

    public function __construct()
    {
        parent::__construct(oneOf(self::DIGITS));
    }

    public function getDescription(): string
    {
        return "One digit";
    }
}
