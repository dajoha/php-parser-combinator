<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Character;

function alpha(): Alpha
{
    return new Alpha();
}

function digit(): Digit
{
    return new Digit();
}

function eol(): Eol
{
    return new Eol();
}
