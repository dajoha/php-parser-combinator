<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Map;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\Context\ContextAwareInterface;
use Dajoha\ParserCombinator\Stream\Base\Span\SpanAwareInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Map extends AbstractParser
{
    public readonly ParserInterface $parser;

    /*
     * @var callable(mixed $output, ParseResult $result, StreamInterface $initialStream): mixed
     */
    public readonly mixed $mapper;

    /**
     * @param callable(mixed, ParseResult): mixed $mapper
     */
    public function __construct(ParserInterface|array|string $parser, callable $mapper)
    {
        $this->parser = AbstractParser::from($parser);
        $this->mapper = $mapper;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);
        $parseResult = $this->parser->parse($stream);

        if ($parseResult->isSuccess()) {
            $output = ($this->mapper)($parseResult->output, $parseResult, $stream);

            if ($output instanceof SpanAwareInterface) {
                $output->setSpan($stream->getSpanTo($parseResult->stream));
            }

            if ($output instanceof ContextAwareInterface) {
                $output->setParseContext($parseResult->stream->getContext());
            }

            return ParseResult::success($parseResult->stream, $output);
        }

        return $parseResult;
    }

    public function getDescription(): string
    {
        return "A map of ".lcfirst($this->parser->getDescription());
    }
}
