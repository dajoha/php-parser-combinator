<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Map;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\Span\SpanAwareInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class MapRes extends AbstractParser
{
    public readonly ParserInterface $parser;

    /** @var callable(ParseResult $result, StreamInterface $initialStream): ParseResult */
    public readonly mixed $mapper;

    /**
     * @param callable(ParseResult): ParseResult $mapper
     */
    public function __construct(ParserInterface|array|string $parser, callable $mapper)
    {
        $this->parser = AbstractParser::from($parser);
        $this->mapper = $mapper;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);
        $parseResult = $this->parser->parse($stream);

        if ($parseResult->isSuccess()) {
            $parseResult2 = ($this->mapper)($parseResult, $stream);
            if ($parseResult2->isSuccess() && $parseResult2->output instanceof SpanAwareInterface) {
                $parseResult2->output->setSpan($stream->getSpanTo($parseResult->stream));
            }

            return $parseResult2->withStream($parseResult->stream);
        } else {
            return $parseResult;
        }
    }

    public function getDescription(): string
    {
        return "A result map of ".lcfirst($this->parser->getDescription());
    }
}
