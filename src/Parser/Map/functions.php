<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Map;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;

/**
 * @param callable(mixed, ParseResult): mixed $mapper
 */
function map(ParserInterface|array|string $parser, callable $mapper): Map
{
    return new Map($parser, $mapper);
}

function mapParser(
    ParserInterface|array|string $parser1,
    ParserInterface|array|string $parser2,
): MapParser
{
    return new MapParser($parser1, $parser2);
}

function mapRes(ParserInterface|array|string $parser, callable $mapper): MapRes
{
    return new MapRes($parser, $mapper);
}

function value(ParserInterface|array|string $parser, mixed $value, bool $clone = false): Value
{
    return new Value($parser, $value, $clone);
}
