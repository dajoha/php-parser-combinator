<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Map;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class MapParser extends AbstractParser
{
    public readonly ParserInterface $parser1;
    public readonly ParserInterface $parser2;

    public function __construct(
        ParserInterface|array|string $parser1,
        ParserInterface|array|string $parser2,
    ) {
        $this->parser1 = AbstractParser::from($parser1);
        $this->parser2 = AbstractParser::from($parser2);
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $parseResult1 = $this->parser1->parse($input);

        if ($parseResult1->isSuccess()) {
            $context1 = $parseResult1->getContext();
            $stream2 = StreamHelper::createStreamFrom($parseResult1->output)->withContext($context1);
            $parseResult2 = $this->parser2->parse($stream2);

            $context2 = $parseResult2->getContext();
            $finalStream = $parseResult1->stream->withContext($context2);

            return $parseResult2->withStream($finalStream);
        } else {
            return $parseResult1;
        }
    }

    public function getDescription(): string
    {
        return "{$this->parser1->getDescription()}, then parsing {$this->parser2->getDescriptionLc()}";
    }
}
