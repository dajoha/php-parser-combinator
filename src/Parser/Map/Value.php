<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Map;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Helper\DescriptionHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Value extends AbstractParser
{
    public readonly ParserInterface $parser;

    public function __construct(
        ParserInterface|array|string $parser,
        public readonly mixed $value,
        public readonly bool $clone = false,
    ) {
        $this->parser = AbstractParser::from($parser);
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $parseResult = $this->parser->parse($input);

        return match ($parseResult->isSuccess()) {
            true => ParseResult::success(
                $parseResult->stream,
                $this->clone ? clone $this->value : $this->value,
            ),
            false => $parseResult,
        };
    }
    public function getDescription(): string
    {
        $value = DescriptionHelper::stringifyMixed($this->value);

        return "{$this->parser->getDescription()} which maps to value $value";
    }
}
