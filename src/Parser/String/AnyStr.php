<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class AnyStr extends AbstractParser
{
    /** @var string[] */
    public readonly array $forbiddenChars;

    public function __construct(
        array|string $forbiddenChars = [],
        public readonly ?string $escapeChar = null,
    ) {
        $this->forbiddenChars = is_string($forbiddenChars) ? mb_str_split($forbiddenChars) : $forbiddenChars;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $output = '';
        $escapeMode = false;

        while (true) {
            $takeResult = $stream->takeOne();
            if ($takeResult === null) {
                break;
            }

            $token = $takeResult->token;

            if ($escapeMode) {
                $output .= $token;
                $escapeMode = false;
            } elseif ($this->escapeChar !== null && $token === $this->escapeChar) {
                $escapeMode = true;
            } elseif (!in_array($token, $this->forbiddenChars, true)) {
                $output .= $token;
            } else {
                break;
            }

            $stream = $takeResult->stream;
        }

        return ParseResult::success($stream, $output);
    }

    public function getDescription(): string
    {
        $forbiddenChars = implode(', ', array_map(fn($t) => "'$t'", $this->forbiddenChars));

        $details = [];
        if (!empty($forbiddenChars)) {
            $details[] = "forbidden chars: $forbiddenChars";
        }
        if ($this->escapeChar !== null) {
            $details[] = "escape char: '$this->escapeChar'";
        }
        $details = empty($details) ? '' : ' ('.implode(', ', $details).')';

        return "A custom string" . $details;
    }
}
