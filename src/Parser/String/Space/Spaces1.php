<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String\Space;

use Dajoha\ParserCombinator\Helper\DescriptionHelper;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\String\space;

class Spaces1 extends InnerParser
{
    public function __construct()
    {
        parent::__construct(many1(space())->ignored());
    }

    public function getDescription(): string
    {
        return DescriptionHelper::ignored("1-or-more spaces/eols");
    }
}
