<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String\Space;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\spaces0;

class RightSpaced extends InnerParser
{
    public readonly ParserInterface $initialParser;

    public function __construct(ParserInterface|array|string $parser)
    {
        $this->initialParser = AbstractParser::from($parser);

        $parser = seq($parser, spaces0())
            ->skipIgnored(false)
            ->map(fn($arr) => $arr[0])
        ;

        parent::__construct($parser);
    }

    public function getDescription(): string
    {
        return "{$this->initialParser->getDescription()} (right-spaced)";
    }
}
