<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String\Space;

use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Token\oneOf;

class Space extends InnerParser
{
    public function __construct()
    {
        parent::__construct(alt("\r\n", oneOf(" \t\n\r")));
    }

    public function getDescription(): string
    {
        return "One space or eol";
    }
}
