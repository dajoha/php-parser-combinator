<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Helper\DescriptionHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Str extends AbstractParser
{
    public function __construct(public readonly string $string)
    {
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        for ($i = 0; $i !== strlen($this->string); $i++) {
            if ($stream->isEof()) {
                return ParseResult::error($stream);
            }
            $takeResult = $stream->takeOne();
            if ($takeResult->token !== $this->string[$i]) {
                return ParseResult::error($stream);
            }
            $stream = $takeResult->stream;
        }

        return ParseResult::success($stream, $this->string);
    }

    public function getDescription(): string
    {
        return DescriptionHelper::stringifyMixed($this->string);
    }
}
