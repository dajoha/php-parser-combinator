<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Combinator\not;
use function Dajoha\ParserCombinator\Parser\Combinator\opt;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;

class FloatNumber extends InnerParser
{
    public function __construct()
    {
        parent::__construct(
            seq(
                opt('-'),
                many1(digit())->toString(),
                alt(
                    seq('.', many1(digit())->toString()),
                    not('.'),
                )
            )->toFloat()
        );
    }

    public function getDescription(): string
    {
        return "One floating number";
    }
}
