<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use ReflectionEnum;
use ReflectionEnumBackedCase;
use ReflectionException;
use RuntimeException;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;

class BackedEnum extends InnerParser
{
    /**
     * @throws ReflectionException
     */
    public function __construct(public readonly string $enumClass)
    {
        $reflectionEnum = new ReflectionEnum($this->enumClass);

        if (!$reflectionEnum->isBacked()) {
            throw new RuntimeException("Enum \"$this->enumClass\" is not backed");
        }

        /** @var ReflectionEnumBackedCase[] $cases */
        $cases = $reflectionEnum->getCases();

        // Sort by backed value string length, the longest ones first:
        usort(
            $cases,
            fn(ReflectionEnumBackedCase $a, ReflectionEnumBackedCase $b) =>
                strlen((string) $b->getBackingValue()) <=> strlen((string) $a->getBackingValue())
        );

        $caseParsers = array_map(
            function(ReflectionEnumBackedCase $case) {
                return str((string) $case->getBackingValue())->value($case->getValue());
            },
            $cases,
        );

        $parser = alt(...$caseParsers);

        parent::__construct($parser);
    }
}
