<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Combinator\opt;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;

class IntNumber extends InnerParser
{
    public function __construct()
    {
        parent::__construct(seq(
            opt('-'),
            many1(digit())->toString(),
        )->toInt());
    }

    public function getDescription(): string
    {
        return "One integer";
    }
}
