<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Token\any;

class AnyChar0 extends InnerParser
{
    /**
     * @param callable(string $char, ParseResult): bool $verifyChar
     */
    public function __construct(callable $verifyChar)
    {
        parent::__construct(many0(any()->verify($verifyChar))->toString());
    }
}
