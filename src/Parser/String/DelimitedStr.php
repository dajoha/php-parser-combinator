<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Parser\Misc\InnerParser;

class DelimitedStr extends InnerParser
{
    public function __construct(
        string $startChar,
        ?string $endChar = null,
        ?string $escapeChar = null,
    ) {
        $endChar ??= $startChar;

        parent::__construct(anyStr($endChar, $escapeChar)->delimitedBy($startChar, $endChar));
    }
}
