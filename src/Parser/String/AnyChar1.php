<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\Token\any;

class AnyChar1 extends InnerParser
{
    /**
     * @param callable(string $char, ParseResult): bool $verifyChar
     */
    public function __construct(callable $verifyChar)
    {
        parent::__construct(many1(any()->verify($verifyChar))->toString());
    }
}
