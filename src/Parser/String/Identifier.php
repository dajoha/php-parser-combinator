<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Character\alpha;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;

class Identifier extends InnerParser
{
    public function __construct()
    {
        parent::__construct(seq(
            alt(alpha(), '_'),
            alt(alpha(), digit(), '_')->many0()->toString(),
        )->toString());
    }

    public function getDescription(): string
    {
        return "One identifier";
    }
}
