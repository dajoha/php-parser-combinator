<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String\Map;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class ToString extends AbstractParser
{
    public readonly ParserInterface $initialParser;
    public readonly ParserInterface $parser;

    public function __construct(ParserInterface|array|string $parser)
    {
        $this->initialParser = AbstractParser::from($parser);

        $this->parser = $this->initialParser->map(function($arr) {
            if (!is_array($arr) && !is_object($arr)) {
                return (string) $arr;
            }

            $flatten = '';
            array_walk_recursive($arr, function ($item) use (&$flatten) {
                $flatten .= $item;
            });

            return $flatten;
        });
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        return $this->parser->parse($input);
    }

    public function getDescription(): string
    {
        return "{$this->initialParser->getDescription()} converted to a string";
    }
}
