<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String\Map;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;

class Trimmed extends InnerParser
{
    public readonly ParserInterface $initialParser;

    public function __construct(
        ParserInterface|array|string $parser,
        string $characters = " \n\r\t\v\0",
    ) {
        $this->initialParser = AbstractParser::from($parser);

        parent::__construct($parser->toString()->map(fn(string $s) => trim($s, $characters)));
    }

    public function getDescription(): string
    {
        return "{$this->initialParser->getDescription()} (trimmed)";
    }
}
