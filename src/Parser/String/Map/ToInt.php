<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\String\Map;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class ToInt extends AbstractParser
{
    public readonly ParserInterface $parser;
    public readonly ParserInterface $initialParser;

    public function __construct(ParserInterface|array|string $parser)
    {
        $this->initialParser = AbstractParser::from($parser);

        $this->parser = $this->initialParser
            ->toString()
            ->verify(fn(string $string) => preg_match('/-?\d+/', $string) === 1)
            ->map(fn($string) => (int) $string)
        ;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        return $this->parser->parse($input);
    }

    public function getDescription(): string
    {
        return "{$this->initialParser->getDescription()} converted to an int";
    }
}
