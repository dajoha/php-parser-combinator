<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\String;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\String\Map\ToFloat;
use Dajoha\ParserCombinator\Parser\String\Map\ToInt;
use Dajoha\ParserCombinator\Parser\String\Map\ToString;
use Dajoha\ParserCombinator\Parser\String\Map\Trimmed;
use Dajoha\ParserCombinator\Parser\String\Space\LeftSpaced;
use Dajoha\ParserCombinator\Parser\String\Space\RightSpaced;
use Dajoha\ParserCombinator\Parser\String\Space\Space;
use Dajoha\ParserCombinator\Parser\String\Space\Spaced;
use Dajoha\ParserCombinator\Parser\String\Space\Spaces0;
use Dajoha\ParserCombinator\Parser\String\Space\Spaces1;
use ReflectionException;

/**
 * @param callable(string $char, ParseResult): bool $verifyChar
 */
function anyChar0(callable $verifyChar): AnyChar0
{
    return new AnyChar0($verifyChar);
}

/**
 * @param callable(string $char, ParseResult): bool $verifyChar
 */
function anyChar1(callable $verifyChar): AnyChar1
{
    return new AnyChar1($verifyChar);
}

function anyStr(array|string $forbiddenChars = [], ?string $escapeChar = null): AnyStr
{
    return new AnyStr($forbiddenChars, $escapeChar);
}

/**
 * @throws ReflectionException
 */
function backedEnum(string $enumClass): BackedEnum
{
    return new BackedEnum($enumClass);
}

function delimitedStr(
    string $startChar,
    ?string $endChar = null,
    ?string $escapeChar = null,
): DelimitedStr {
    return new DelimitedStr($startChar, $endChar, $escapeChar);
}

function float(): FloatNumber {
    return new FloatNumber();
}

function identifier(): Identifier
{
    return new Identifier();
}

function integer(): IntNumber
{
    return new IntNumber();
}

function intNumber(): IntNumber
{
    return new IntNumber();
}

function iStr(string $str): IStr
{
    return new IStr($str);
}

function leftSpaced(ParserInterface|array|string $parser): LeftSpaced
{
    return new LeftSpaced($parser);
}

function rightSpaced(ParserInterface|array|string $parser): RightSpaced
{
    return new RightSpaced($parser);
}

function space(): Space
{
    return new Space();
}

function spaced(ParserInterface|array|string $parser): Spaced
{
    return new Spaced($parser);
}

function spaces0(): Spaces0
{
    return new Spaces0();
}

function spaces1(): Spaces1
{
    return new Spaces1();
}

function str(string $str): Str
{
    return new Str($str);
}

function toFloat(ParserInterface|array|string $parser): ToFloat
{
    return new ToFloat($parser);
}

function toInt(ParserInterface|array|string $parser): ToInt
{
    return new ToInt($parser);
}

function toString(ParserInterface|array|string $parser): ToString
{
    return new ToString($parser);
}

function trimmed(
    ParserInterface|array|string $parser,
    string $characters = " \n\r\t\v\0",
): Trimmed {
    return new Trimmed($parser, $characters);
}
