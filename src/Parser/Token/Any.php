<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Token;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Any extends AbstractParser
{
    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $takeResult = $stream->takeOne();

        if ($takeResult === null) {
            return ParseResult::error($stream);
        }

        return ParseResult::success($takeResult->stream, $takeResult->token);
    }

    public function getDescription(): string
    {
        return "Any token";
    }
}
