<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Token;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class One extends AbstractParser
{
    public function __construct(public readonly mixed $token)
    {
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $takeResult = $stream->takeOne();

        if ($takeResult === null || $takeResult->token !== $this->token) {
            return ParseResult::error($takeResult->stream ?? $stream); // TODO: use $takeResult->stream instead of $stream, do it for all parsers in this directory
        }

        return ParseResult::success($takeResult->stream, $takeResult->token);
    }

    public function getDescription(): string
    {
        return "One token '$this->token'";
    }
}
