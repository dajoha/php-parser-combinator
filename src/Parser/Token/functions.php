<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Token;

function any(): Any
{
    return new Any();
}

function noneOf(array|string $tokens): NoneOf
{
    return new NoneOf($tokens);
}

function notOne(mixed $token): NotOne
{
    return new NotOne($token);
}

function one(mixed $token): One
{
    return new One($token);
}

function oneOf(array|string $tokens): OneOf
{
    return new OneOf($tokens);
}
