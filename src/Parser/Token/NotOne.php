<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Token;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class NotOne extends AbstractParser
{
    public function __construct(public readonly mixed $token)
    {
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $takeResult = $stream->takeOne();

        if ($takeResult === null || $takeResult->token === $this->token) {
            return ParseResult::error($stream);
        }

        return ParseResult::success($takeResult->stream, $takeResult->token);
    }

    public function getDescription(): string
    {
        return "Not one token '$this->token'";
    }
}
