<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Token;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Helper\DescriptionHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class NoneOf extends AbstractParser
{
    public readonly array $tokens;

    public function __construct(array|string $tokens)
    {
        $this->tokens = is_string($tokens) ? mb_str_split($tokens) : $tokens;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $takeResult = $stream->takeOne();

        if ($takeResult === null) {
            return ParseResult::error($stream);
        }

        if (in_array($takeResult->token, $this->tokens, true)) {
            return ParseResult::error($stream);
        }

        return ParseResult::success($takeResult->stream, $takeResult->token);
    }

    public function getDescription(): string
    {
        $tokens = implode(', ', array_map(DescriptionHelper::stringifyMixed(...), $this->tokens));

        return "None of [$tokens]";
    }
}
