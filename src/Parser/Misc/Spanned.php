<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Misc;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Misc\OutputWrapper\SpannedValue;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Spanned extends AbstractParser
{
    public function __construct(public readonly ParserInterface $parser)
    {
    }

    public function parse(StreamInterface|array|string $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $parseResult = $this->parser->parse($stream);
        if (!$parseResult->isSuccess()) {
            return $parseResult;
        }

        $span = $stream->getSpanTo($parseResult->stream);
        $output = new SpannedValue($parseResult->output, $span);

        return $parseResult->withOutput($output);
    }
}
