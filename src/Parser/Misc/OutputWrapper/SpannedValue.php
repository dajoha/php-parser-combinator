<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Misc\OutputWrapper;

use Dajoha\ParserCombinator\Stream\Base\Span\SpanInterface;

/**
 * @template P
 */
readonly class SpannedValue
{
    public function __construct(
        public mixed $value,
        /** @var SpanInterface<P> */
        public SpanInterface $span,
    ) {
    }
}
