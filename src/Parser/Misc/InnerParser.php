<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Misc;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class InnerParser extends AbstractParser
{
    public function __construct(public readonly ParserInterface $parser)
    {
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        return $this->parser->parse($input);
    }

    public function getDescription(): string
    {
        return $this->parser->getDescription();
    }
}
