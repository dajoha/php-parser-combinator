<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Misc;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Recursive extends AbstractParser
{
    protected static int $nextRecursiveParserId = 1;

    public readonly int $recursiveParserId;

    protected bool $recursiveDescription = false;

    public function __construct(public ParserInterface $parser = new Nothing())
    {
        $this->recursiveParserId = self::$nextRecursiveParserId++;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        return $this->parser->parse($input);
    }

    public function setParser(ParserInterface $parser): Recursive
    {
        $this->parser = $parser;

        return $this;
    }

    public function getDescription(): string
    {
        if ($this->recursiveDescription) {
            return "[Recursive parser #$this->recursiveParserId]";
        }

        $this->recursiveDescription = true;
        $description = "[Recursive parser #$this->recursiveParserId = ] " . $this->parser->getDescription();
        // $this->recursiveDescription = false;

        return $description;
    }
}
