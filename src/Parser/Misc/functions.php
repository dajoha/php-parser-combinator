<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Misc;

use Dajoha\ParserCombinator\Base\ParserInterface;

function complete(ParserInterface|array|string $parser): Complete
{
    return new Complete($parser);
}

function descriptionLimiter(ParserInterface $parser): DescriptionLimiter
{
    return new DescriptionLimiter($parser);
}

function innerParser(ParserInterface $parser): InnerParser
{
    return new InnerParser($parser);
}

function nothing(): Nothing
{
    return new Nothing();
}

function recursive(ParserInterface $parser = new Nothing()): Recursive
{
    return new Recursive($parser);
}

function spanned(ParserInterface $parser): Spanned
{
    return new Spanned($parser);
}
