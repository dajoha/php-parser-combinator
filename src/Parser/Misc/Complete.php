<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Misc;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Complete extends AbstractParser
{
    public readonly ParserInterface $parser;

    public function __construct(ParserInterface|array|string $parser)
    {
        $this->parser = AbstractParser::from($parser);
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $parseResult = $this->parser->parse($input);
        if (!$parseResult->isSuccess()) {
            return $parseResult;
        }

        if (!$parseResult->stream->isEof()) {
            return ParseResult::error($parseResult->stream);
        }

        return $parseResult;
    }

    public function getDescription(): string
    {
        return "{$this->parser->getDescription()} with no remaining input";
    }
}
