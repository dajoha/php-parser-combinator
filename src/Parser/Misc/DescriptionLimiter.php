<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Misc;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

/**
 * TODO: WIP
 */
class DescriptionLimiter extends AbstractParser
{
    protected static int $nextParserId = 1;

    public readonly int $parserId;

    protected bool $recursiveDescription = false;

    public static int $count=0;

    public function __construct(public ParserInterface $parser = new Nothing())
    {
        $this->parserId = self::$nextParserId++;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        self::$count++;
        return $this->parser->parse($input);
    }

    public function setParser(ParserInterface $parser): DescriptionLimiter
    {
        $this->parser = $parser;

        return $this;
    }

    public function getDescription(): string
    {
        if ($this->recursiveDescription) {
            return "[Parser #$this->parserId]";
        }

        $this->recursiveDescription = true;
        $description = "[Parser #$this->parserId = ] " . $this->parser->getDescription();
        // $this->recursiveDescription = false;

        return $description;
    }
}
