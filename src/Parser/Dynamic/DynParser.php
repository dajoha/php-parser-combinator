<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Dynamic;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class DynParser extends AbstractParser
{
    /** @var callable(StreamInterface): ParserInterface */
    public readonly mixed $parserProvider;

    /**
     * @param callable(StreamInterface): ParserInterface $parserProvider
     */
    public function __construct(callable $parserProvider)
    {
        $this->parserProvider = $parserProvider;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $parser = ($this->parserProvider)($stream);

        return $parser->parse($stream);
    }

    public function getDescription(): string
    {
        return "A dynamically parsed input (DynParser)";
    }
}
