<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Dynamic;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

/**
 * @param callable(StreamInterface, ParserInterface|null $optParser): ParseResult $parserCallback
 */
function dyn(
    callable $parserCallback,
    ParserInterface|array|string|null $optParser = null,
): Dyn {
    return new Dyn($parserCallback, $optParser);
}

/**
 * @param callable(StreamInterface): ParserInterface $parserProvider
 */
function dynParser(callable $parserProvider): DynParser
{
    return new DynParser($parserProvider);
}
