<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Dynamic;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Dyn extends AbstractParser
{
    /** @var callable(StreamInterface, ParserInterface|null $optParser): ParseResult */
    public readonly mixed $parserCallback;

    public readonly ?ParserInterface $optParser;

    /**
     * @param callable(StreamInterface, ParserInterface|null $optParser): ParseResult $parserCallback
     */
    public function __construct(
        callable $parserCallback,
        ParserInterface|array|string|null $optParser = null,
    ) {
        $this->parserCallback = $parserCallback;
        $this->optParser = $optParser === null ? null : AbstractParser::from($optParser);
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        return ($this->parserCallback)($stream, $this->optParser);
    }

    public function getDescription(): string
    {
        $description = "A dynamically parsed input";
        if ($this->optParser !== null) {
            $description .= ", based on {$this->optParser->getDescriptionLc()}";
        }

        return $description;
    }
}
