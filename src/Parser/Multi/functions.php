<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Parser\Multi;

use Dajoha\ParserCombinator\Base\ParserInterface;

function many0(ParserInterface|array|string $parser): Many0
{
    return new Many0($parser);
}

function many1(ParserInterface|array|string $parser): Many1
{
    return new Many1($parser);
}

function manyN(int $n, ParserInterface|array|string $parser): ManyN
{
    return new ManyN($n, $parser);
}

function manyNM(int $n, int $m, ParserInterface|array|string $parser): ManyNM
{
    return new ManyNM($n, $m, $parser);
}

function separated0(
    ParserInterface|array|string $parser,
    ParserInterface|array|string $separator,
): Separated0 {
    return new Separated0($parser, $separator);
}

function separated1(
    ParserInterface|array|string $parser,
    ParserInterface|array|string $separator,
): Separated1 {
    return new Separated1($parser, $separator);
}
