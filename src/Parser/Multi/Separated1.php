<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Multi;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Parser\Sequence\Preceded;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class Separated1 extends AbstractParser
{
    public readonly ParserInterface $parser;

    public readonly ParserInterface $separator;

    public bool $skipIgnored = true;

    protected Preceded $extraElementParser;

    public function __construct(
        ParserInterface|array|string $parser,
        ParserInterface|array|string $separator,
    ) {
        $this->parser = AbstractParser::from($parser);
        $this->separator = AbstractParser::from($separator);
        $this->extraElementParser = $this->parser->precededBy($this->separator);
    }

    public function skipIgnored(bool $skipIgnored): self
    {
        $this->skipIgnored = $skipIgnored;

        return $this;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $output = [];

        $parseResult = $this->parser->parse($stream);
        switch ($parseResult->status) {
            case ParseStatus::Success: break;
            default: return $parseResult; // TODO: give context for fatal status
        }

        do {
            if (!$this->skipIgnored || !$parseResult->output instanceof IgnoredOutput) {
                $output[] = $parseResult->output;
            }
            $stream = $parseResult->stream;
            $parseResult = $this->extraElementParser->parse($stream);
        } while ($parseResult->isSuccess());

        if ($parseResult->isFatal()) {
            return $parseResult; // TODO: give context
        }

        return ParseResult::success($stream, $output);
    }

    public function getDescription(): string
    {
        return "1-or-more of {$this->parser->getDescriptionLc()} separated by {$this->separator->getDescriptionLc()}";
    }
}
