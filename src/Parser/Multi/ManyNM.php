<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Multi;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use RuntimeException;

class ManyNM extends AbstractParser
{
    public readonly ParserInterface $parser;

    public bool $skipIgnored = true;

    public function __construct(
        public readonly int $n,
        public readonly int $m,
        ParserInterface|array|string $parser,
    ) {
        if ($n > $m) {
            throw new RuntimeException('$n must not be greater then $m');
        }
        $this->parser = AbstractParser::from($parser);
    }

    public function skipIgnored(bool $skipIgnored): self
    {
        $this->skipIgnored = $skipIgnored;

        return $this;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $output = [];

        $advance = function() use(&$stream, &$output) {
            $parseResult = $this->parser->parse($stream);

            switch ($parseResult->status) {
                case ParseStatus::Success: break;
                default: return $parseResult; // TODO: give context
            }

            if (!$this->skipIgnored || !$parseResult->output instanceof IgnoredOutput) {
                $output[] = $parseResult->output;
            }
            $stream = $parseResult->stream;

            return null;
        };

        for ($i = 0; $i < $this->n; $i++) {
            $parseResult = $advance();
            if ($parseResult !== null) {
                return $parseResult;
            }
        }

        for ($i = $this->n; $i < $this->m; $i++) {
            $parseResult = $advance();
            switch ($parseResult?->status) {
                case ParseStatus::Success: break;
                case ParseStatus::Error: break 2;
                case ParseStatus::Fatal: return $parseResult;
            }
        }

        return ParseResult::success($stream, $output);
    }

    public function getDescription(): string
    {
        return "Between $this->n and $this->m of {$this->parser->getDescriptionLc()}";
    }
}
