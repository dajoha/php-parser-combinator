<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Parser\Multi;

use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

class ManyN extends AbstractParser
{
    public readonly ParserInterface $parser;

    public bool $skipIgnored = true;

    public function __construct(
        public readonly int $n,
        ParserInterface|array|string $parser,
    ) {
        $this->parser = AbstractParser::from($parser);
    }

    public function skipIgnored(bool $skipIgnored): self
    {
        $this->skipIgnored = $skipIgnored;

        return $this;
    }

    public function parse(StreamInterface|string|array $input): ParseResult
    {
        $stream = StreamHelper::createStreamFrom($input);

        $output = [];

        for ($i = 0; $i < $this->n; $i++) {
            $parseResult = $this->parser->parse($stream);

            switch ($parseResult->status) {
                case ParseStatus::Success: break;
                default: return $parseResult; // TODO: give context
            }

            if (!$this->skipIgnored || !$parseResult->output instanceof IgnoredOutput) {
                $output[] = $parseResult->output;
            }
            $stream = $parseResult->stream;
        }

        return ParseResult::success($stream, $output);
    }

    public function getDescription(): string
    {
        return "$this->n of {$this->parser->getDescriptionLc()}";
    }
}
