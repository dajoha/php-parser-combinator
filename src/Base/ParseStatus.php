<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Base;

enum ParseStatus
{
    case Success;
    case Error;
    case Fatal;

    public function isSuccess(): bool
    {
        return $this === self::Success;
    }

    public function isError(): bool
    {
        return $this === self::Error;
    }

    public function isFatal(): bool
    {
        return $this === self::Fatal;
    }
}
