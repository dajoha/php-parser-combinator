<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Base;

use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

readonly class ParseResult
{
    public mixed $output;

    public function __construct(
        public ParseStatus $status,
        public StreamInterface $stream,
        mixed $output = null,
        public ?string $fatalContextMessage = null,
    ) {
        if (!$this->status->isSuccess()) {
            $output = null;
        }
        $this->output = $output;
    }

    public static function success(StreamInterface $stream, mixed $output): self
    {
        return new self(ParseStatus::Success, $stream, $output);
    }

    public static function error(StreamInterface $stream): self
    {
        return new self(ParseStatus::Error, $stream);
    }

    public static function fatal(StreamInterface $stream, ?string $fatalContextMessage = null): self
    {
        return new self(
            ParseStatus::Fatal,
            $stream,
            fatalContextMessage: $fatalContextMessage,
        );
    }

    public function withStream(StreamInterface $stream): ParseResult
    {
        return new ParseResult($this->status, $stream, $this->output, $this->fatalContextMessage);
    }

    public function withOutput(mixed $output): ParseResult
    {
        return new ParseResult($this->status, $this->stream, $output, $this->fatalContextMessage);
    }

    public function withContext(mixed $context): ParseResult
    {
        $stream = $this->stream->withContext($context);

        return new ParseResult($this->status, $stream, $this->output, $this->fatalContextMessage);
    }

    public function isSuccess(): bool
    {
        return $this->status->isSuccess();
    }

    public function isError(): bool
    {
        return $this->status->isError();
    }

    public function isFatal(): bool
    {
        return $this->status->isFatal();
    }

    public function getContext(): mixed
    {
        return $this->stream->getContext();
    }
}
