<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Base;

use Dajoha\ParserCombinator\Parser\Combinator\Not;
use Dajoha\ParserCombinator\Parser\Combinator\Opt;
use Dajoha\ParserCombinator\Parser\Combinator\OrFatal;
use Dajoha\ParserCombinator\Parser\Combinator\Verify;
use Dajoha\ParserCombinator\Parser\Context\AlterContext;
use Dajoha\ParserCombinator\Parser\Context\WithSubContext;
use Dajoha\ParserCombinator\Parser\Debug\Debug;
use Dajoha\ParserCombinator\Parser\Debug\Dump;
use Dajoha\ParserCombinator\Parser\Debug\DumpOutput;
use Dajoha\ParserCombinator\Parser\Dynamic\Dyn;
use Dajoha\ParserCombinator\Parser\Map\Map;
use Dajoha\ParserCombinator\Parser\Map\MapParser;
use Dajoha\ParserCombinator\Parser\Map\MapRes;
use Dajoha\ParserCombinator\Parser\Map\Value;
use Dajoha\ParserCombinator\Parser\Misc\Complete;
use Dajoha\ParserCombinator\Parser\Misc\Spanned;
use Dajoha\ParserCombinator\Parser\Multi\Many0;
use Dajoha\ParserCombinator\Parser\Multi\Many1;
use Dajoha\ParserCombinator\Parser\Multi\ManyN;
use Dajoha\ParserCombinator\Parser\Multi\ManyNM;
use Dajoha\ParserCombinator\Parser\Multi\Separated0;
use Dajoha\ParserCombinator\Parser\Multi\Separated1;
use Dajoha\ParserCombinator\Parser\Sequence\Delimited;
use Dajoha\ParserCombinator\Parser\Sequence\Followed;
use Dajoha\ParserCombinator\Parser\Sequence\Ignored;
use Dajoha\ParserCombinator\Parser\Sequence\Preceded;
use Dajoha\ParserCombinator\Parser\String\Map\ToFloat;
use Dajoha\ParserCombinator\Parser\String\Map\ToInt;
use Dajoha\ParserCombinator\Parser\String\Map\ToString;
use Dajoha\ParserCombinator\Parser\String\Map\Trimmed;
use Dajoha\ParserCombinator\Parser\String\Space\LeftSpaced;
use Dajoha\ParserCombinator\Parser\String\Space\RightSpaced;
use Dajoha\ParserCombinator\Parser\String\Space\Spaced;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;

interface ParserInterface
{
    public function parse(StreamInterface|string|array $input): ParseResult;

    public function getDescription(): string;

    public function getDescriptionLc(): string;

    /**
     * @param callable(ParseResult): mixed $contextProvider
     */
    public function alterContext(callable $contextProvider): AlterContext;

    /**
     * @param callable(ParseResult, StreamInterface): void $callback
     */
    public function debug(callable $callback): Debug;

    public function delimitedBy(
        ParserInterface|array|string $left,
        ParserInterface|array|string $right,
    ): Delimited;

    /**
     * @param (callable(ParseResult): mixed)|null $parseResultMapper
     */
    public function dump(?callable $parseResultMapper = null): Dump;

    public function dumpOutput(): DumpOutput;

    /**
     * @param callable(StreamInterface, ParserInterface|null $optParser): ParseResult $parserCallback
     */
    public function dyn(callable $parserCallback): Dyn;

    public function complete(): Complete;

    public function followedBy(ParserInterface|array|string $parser): Followed;

    public function ignored(): Ignored;

    public function leftSpaced(): LeftSpaced;

    public function many0(): Many0;

    public function many1(): Many1;

    public function manyN(int $n): ManyN;

    public function manyNM(int $n, int $m): ManyNM;

    /**
     * @param callable(mixed, ParseResult): mixed $mapper
     */
    public function map(callable $mapper): Map;

    public function mapParser(ParserInterface|array|string $parser): MapParser;

    /**
     * @param callable(ParseResult): ParseResult $mapper
     */
    public function mapRes(callable $mapper): MapRes;

    public function not(): Not;

    public function opt(): Opt;

    public function orFatal(?string $fatalContextMessage = null): OrFatal;

    public function precededBy(ParserInterface|array|string $parser): Preceded;

    public function rightSpaced(): RightSpaced;

    public function separated0By(ParserInterface|array|string $separator): Separated0;

    public function separated1By(ParserInterface|array|string $separator): Separated1;

    public function spaced(): Spaced;

    public function spanned(): Spanned;

    public function toFloat(): ToFloat;

    public function toInt(): ToInt;

    public function toString(): ToString;

    public function trimmed(string $characters = " \n\r\t\v\0"): Trimmed;

    public function value(mixed $value, bool $clone = false): Value;

    /**
     * @param  callable(mixed, ParseResult): bool $callback
     */
    public function verify(callable $callback): Verify;

    public function withSubContext(mixed $subContext): WithSubContext;
}
