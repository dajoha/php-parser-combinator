<?php

/** @noinspection PhpUnused */

namespace Dajoha\ParserCombinator\Base;

use Dajoha\ParserCombinator\Parser\Character\Alpha;
use Dajoha\ParserCombinator\Parser\Character\Digit;
use Dajoha\ParserCombinator\Parser\Character\Eol;
use Dajoha\ParserCombinator\Parser\Combinator\Alt;
use Dajoha\ParserCombinator\Parser\Combinator\Not;
use Dajoha\ParserCombinator\Parser\Combinator\Opt;
use Dajoha\ParserCombinator\Parser\Combinator\OrFatal;
use Dajoha\ParserCombinator\Parser\Combinator\Verify;
use Dajoha\ParserCombinator\Parser\Context\AlterContext;
use Dajoha\ParserCombinator\Parser\Context\WithSubContext;
use Dajoha\ParserCombinator\Parser\Debug\Debug;
use Dajoha\ParserCombinator\Parser\Dynamic\Dyn;
use Dajoha\ParserCombinator\Parser\Dynamic\DynParser;
use Dajoha\ParserCombinator\Parser\Map\Map;
use Dajoha\ParserCombinator\Parser\Map\MapParser;
use Dajoha\ParserCombinator\Parser\Map\MapRes;
use Dajoha\ParserCombinator\Parser\Map\Value;
use Dajoha\ParserCombinator\Parser\Misc\Complete;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use Dajoha\ParserCombinator\Parser\Misc\Nothing;
use Dajoha\ParserCombinator\Parser\Misc\Recursive;
use Dajoha\ParserCombinator\Parser\Misc\Spanned;
use Dajoha\ParserCombinator\Parser\Multi\Many0;
use Dajoha\ParserCombinator\Parser\Multi\Many1;
use Dajoha\ParserCombinator\Parser\Multi\ManyN;
use Dajoha\ParserCombinator\Parser\Multi\ManyNM;
use Dajoha\ParserCombinator\Parser\Multi\Separated0;
use Dajoha\ParserCombinator\Parser\Multi\Separated1;
use Dajoha\ParserCombinator\Parser\Sequence\Delimited;
use Dajoha\ParserCombinator\Parser\Sequence\Followed;
use Dajoha\ParserCombinator\Parser\Sequence\Ignored;
use Dajoha\ParserCombinator\Parser\Sequence\Preceded;
use Dajoha\ParserCombinator\Parser\Sequence\Seq;
use Dajoha\ParserCombinator\Parser\String\AnyChar0;
use Dajoha\ParserCombinator\Parser\String\AnyChar1;
use Dajoha\ParserCombinator\Parser\String\AnyStr;
use Dajoha\ParserCombinator\Parser\String\BackedEnum;
use Dajoha\ParserCombinator\Parser\String\DelimitedStr;
use Dajoha\ParserCombinator\Parser\String\Identifier;
use Dajoha\ParserCombinator\Parser\String\IntNumber;
use Dajoha\ParserCombinator\Parser\String\IStr;
use Dajoha\ParserCombinator\Parser\String\Map\ToFloat;
use Dajoha\ParserCombinator\Parser\String\Map\ToInt;
use Dajoha\ParserCombinator\Parser\String\Map\ToString;
use Dajoha\ParserCombinator\Parser\String\Map\Trimmed;
use Dajoha\ParserCombinator\Parser\String\Space\LeftSpaced;
use Dajoha\ParserCombinator\Parser\String\Space\RightSpaced;
use Dajoha\ParserCombinator\Parser\String\Space\Space;
use Dajoha\ParserCombinator\Parser\String\Space\Spaced;
use Dajoha\ParserCombinator\Parser\String\Space\Spaces0;
use Dajoha\ParserCombinator\Parser\String\Space\Spaces1;
use Dajoha\ParserCombinator\Parser\String\Str;
use Dajoha\ParserCombinator\Parser\Token\Any;
use Dajoha\ParserCombinator\Parser\Token\NoneOf;
use Dajoha\ParserCombinator\Parser\Token\NotOne;
use Dajoha\ParserCombinator\Parser\Token\One;
use Dajoha\ParserCombinator\Parser\Token\OneOf;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use ReflectionException;

/**
 * Just an alternative way to create parsers.
 */
class Parser {
    public static function alpha(): Alpha
    {
        return new Alpha();
    }

    public static function alt(ParserInterface|array|string ...$parsers): Alt
    {
        return new Alt(...$parsers);
    }

    /**
     * @param callable(ParseResult): mixed $contextProvider
     */
    public static function alterContext(
        ParserInterface|array|string $parser,
        callable $contextProvider,
    ): AlterContext
    {
        return new AlterContext($parser, $contextProvider);
    }

    public static function any(): Any
    {
        return new Any();
    }

    /**
     * @param callable(string $char, ParseResult): bool $verifyChar
     */
    public static function anyChar0(callable $verifyChar): AnyChar0
    {
        return new AnyChar0($verifyChar);
    }

    /**
     * @param callable(string $char, ParseResult): bool $verifyChar
     */
    public static function anyChar1(callable $verifyChar): AnyChar1
    {
        return new AnyChar1($verifyChar);
    }

    public static function anyStr(
        array|string $forbiddenChars = [],
        ?string $escapeChar = null,
    ): AnyStr
    {
        return new AnyStr($forbiddenChars, $escapeChar);
    }

    /**
     * @throws ReflectionException
     */
    public static function backedEnum(string $enumClass): BackedEnum
    {
        return new BackedEnum($enumClass);
    }

    /**
     * @param callable(ParseResult, StreamInterface): void $callback
     */
    public static function debug(ParserInterface|array|string $parser, callable $callback): Debug
    {
        return new Debug($parser, $callback);
    }

    public static function delimited(
        ParserInterface|array|string $left,
        ParserInterface|array|string $value,
        ParserInterface|array|string $right,
    ): Delimited
    {
        return new Delimited($left, $value, $right);
    }

    public static function delimitedStr(
        string $startChar,
        ?string $endChar = null,
        ?string $escapeChar = null,
    ): DelimitedStr {
        return new DelimitedStr($startChar, $endChar, $escapeChar);
    }

    public static function digit(): Digit
    {
        return new Digit();
    }

    /**
     * @param callable(StreamInterface, ParserInterface|null $optParser): ParseResult $parserCallback
     */
    public static function dyn(
        callable $parserCallback,
        ParserInterface|array|string|null $optParser = null,
    ): Dyn {
        return new Dyn($parserCallback, $optParser);
    }

    /**
     * @param callable(StreamInterface): ParserInterface $parserProvider
     */
    public static function dynParser(callable $parserProvider): DynParser
    {
        return new DynParser($parserProvider);
    }

    public static function complete(ParserInterface|array|string $parser): Complete
    {
        return new Complete($parser);
    }

    public static function eol(): Eol
    {
        return new Eol();
    }

    public static function followed(
        ParserInterface|array|string $value,
        ParserInterface|array|string $right,
    ): Followed
    {
        return new Followed($value, $right);
    }

    public static function identifier(): Identifier
    {
        return new Identifier();
    }

    public static function ignored(ParserInterface|array|string $parser): Ignored
    {
        return new Ignored($parser);
    }

    public static function innerParser(ParserInterface $parser): InnerParser
    {
        return new InnerParser($parser);
    }

    public static function integer(): IntNumber
    {
        return new IntNumber();
    }

    public static function intNumber(): IntNumber
    {
        return new IntNumber();
    }

    public static function iStr(string $str): IStr
    {
        return new IStr($str);
    }

    public static function leftSpaced(ParserInterface|array|string $parser): LeftSpaced
    {
        return new LeftSpaced($parser);
    }

    public static function many0(ParserInterface|array|string $parser): Many0
    {
        return new Many0($parser);
    }

    public static function many1(ParserInterface|array|string $parser): Many1
    {
        return new Many1($parser);
    }

    public static function manyN(int $n, ParserInterface|array|string $parser): ManyN
    {
        return new ManyN($n, $parser);
    }

    public static function manyNM(int $n, int $m, ParserInterface|array|string $parser): ManyNM
    {
        return new ManyNM($n, $m, $parser);
    }

    /**
     * @param callable(mixed, ParseResult): mixed $mapper
     */
    public static function map(ParserInterface|array|string $parser, callable $mapper): Map
    {
        return new Map($parser, $mapper);
    }

    public static function mapParser(
        ParserInterface|array|string $parser1,
        ParserInterface|array|string $parser2,
    ): MapParser
    {
        return new MapParser($parser1, $parser2);
    }

    public static function mapRes(ParserInterface|array|string $parser, callable $mapper): MapRes
    {
        return new MapRes($parser, $mapper);
    }

    public static function noneOf(array|string $tokens): NoneOf
    {
        return new NoneOf($tokens);
    }

    public static function not(ParserInterface|array|string $parser): Not
    {
        return new Not($parser);
    }

    public static function nothing(): Nothing
    {
        return new Nothing();
    }

    public static function notOne(mixed $token): NotOne
    {
        return new NotOne($token);
    }

    public static function one(mixed $token): One
    {
        return new One($token);
    }

    public static function oneOf(array|string $tokens): OneOf
    {
        return new OneOf($tokens);
    }

    public static function opt(ParserInterface|array|string $parser): Opt
    {
        return new Opt($parser);
    }

    public static function orFatal(ParserInterface|array|string $parser): OrFatal
    {
        return new OrFatal($parser);
    }

    public static function preceded(
        ParserInterface|array|string $left,
        ParserInterface|array|string $value,
    ): Preceded
    {
        return new Preceded($left, $value);
    }

    public static function recursive(ParserInterface $parser): Recursive
    {
        return new Recursive($parser);
    }

    public static function rightSpaced(ParserInterface|array|string $parser): RightSpaced
    {
        return new RightSpaced($parser);
    }

    public static function separated0(
        ParserInterface|array|string $parser,
        ParserInterface|array|string $separator,
    ): Separated0 {
        return new Separated0($parser, $separator);
    }

    public static function separated1(
        ParserInterface|array|string $parser,
        ParserInterface|array|string $separator,
    ): Separated1 {
        return new Separated1($parser, $separator);
    }

    public static function seq(ParserInterface|array|string ...$parsers): Seq
    {
        return new Seq(...$parsers);
    }

    public static function space(): Space
    {
        return new Space();
    }

    public static function spaced(ParserInterface|array|string $parser): Spaced
    {
        return new Spaced($parser);
    }

    public static function spaces0(): Spaces0
    {
        return new Spaces0();
    }

    public static function spaces1(): Spaces1
    {
        return new Spaces1();
    }

    public static function spanned(ParserInterface $parser): Spanned
    {
        return new Spanned($parser);
    }

    public static function str(string $str): Str
    {
        return new Str($str);
    }

    public static function toFloat(ParserInterface|array|string $parser): ToFloat
    {
        return new ToFloat($parser);
    }

    public static function toInt(ParserInterface|array|string $parser): ToInt
    {
        return new ToInt($parser);
    }

    public static function toString(ParserInterface|array|string $parser): ToString
    {
        return new ToString($parser);
    }

    public static function trimmed(
        ParserInterface|array|string $parser,
        string $characters = " \n\r\t\v\0",
    ): Trimmed {
        return new Trimmed($parser, $characters);
    }

    public static function value(
        ParserInterface|array|string $parser,
        mixed $value,
        bool $clone = false,
    ): Value
    {
        return new Value($parser, $value, $clone);
    }

    /**
     * @param  callable(mixed, ParseResult): bool $callback
     */
    public static function verify(ParserInterface|array|string $parser, callable $callback): Verify
    {
        return new Verify($parser, $callback);
    }

    function withSubContext(mixed $subContext, ParserInterface|array|string $parser): WithSubContext
    {
        return new WithSubContext($subContext, $parser);
    }
}
