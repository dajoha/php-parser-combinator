<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Base;

use Dajoha\ParserCombinator\Parser\Combinator\Not;
use Dajoha\ParserCombinator\Parser\Combinator\Opt;
use Dajoha\ParserCombinator\Parser\Combinator\OrFatal;
use Dajoha\ParserCombinator\Parser\Combinator\Verify;
use Dajoha\ParserCombinator\Parser\Context\AlterContext;
use Dajoha\ParserCombinator\Parser\Context\WithSubContext;
use Dajoha\ParserCombinator\Parser\Debug\Debug;
use Dajoha\ParserCombinator\Parser\Debug\Dump;
use Dajoha\ParserCombinator\Parser\Debug\DumpOutput;
use Dajoha\ParserCombinator\Parser\Dynamic\Dyn;
use Dajoha\ParserCombinator\Parser\Map\Map;
use Dajoha\ParserCombinator\Parser\Map\MapParser;
use Dajoha\ParserCombinator\Parser\Map\MapRes;
use Dajoha\ParserCombinator\Parser\Map\Value;
use Dajoha\ParserCombinator\Parser\Misc\Complete;
use Dajoha\ParserCombinator\Parser\Misc\Spanned;
use Dajoha\ParserCombinator\Parser\Multi\Many0;
use Dajoha\ParserCombinator\Parser\Multi\Many1;
use Dajoha\ParserCombinator\Parser\Multi\ManyN;
use Dajoha\ParserCombinator\Parser\Multi\ManyNM;
use Dajoha\ParserCombinator\Parser\Multi\Separated0;
use Dajoha\ParserCombinator\Parser\Multi\Separated1;
use Dajoha\ParserCombinator\Parser\Sequence\Delimited;
use Dajoha\ParserCombinator\Parser\Sequence\Followed;
use Dajoha\ParserCombinator\Parser\Sequence\Ignored;
use Dajoha\ParserCombinator\Parser\Sequence\Preceded;
use Dajoha\ParserCombinator\Parser\Sequence\Seq;
use Dajoha\ParserCombinator\Parser\String\Map\ToFloat;
use Dajoha\ParserCombinator\Parser\String\Map\ToInt;
use Dajoha\ParserCombinator\Parser\String\Map\ToString;
use Dajoha\ParserCombinator\Parser\String\Map\Trimmed;
use Dajoha\ParserCombinator\Parser\String\Space\LeftSpaced;
use Dajoha\ParserCombinator\Parser\String\Space\RightSpaced;
use Dajoha\ParserCombinator\Parser\String\Space\Spaced;
use Dajoha\ParserCombinator\Parser\String\Str;

abstract class AbstractParser implements ParserInterface
{
    public function getDescription(): string
    {
        return "(no description)";
    }

    public function getDescriptionLc(): string
    {
        return lcfirst($this->getDescription());
    }

    public static function from(ParserInterface|array|string $parser): ParserInterface
    {
        if (is_string($parser)) {
            return new Str($parser);
        } elseif (is_array($parser)) {
            return new Seq(...$parser);
        } else {
            return $parser;
        }
    }

    /**
     * @param callable(ParseResult): mixed $contextProvider
     */
    public function alterContext(callable $contextProvider): AlterContext
    {
        return new AlterContext($this, $contextProvider);
    }

    public function debug(callable $callback): Debug
    {
        return new Debug($this, $callback);
    }

    public function dump(?callable $parseResultMapper = null): Dump
    {
        return new Dump($this, $parseResultMapper);
    }

    public function dumpOutput(): DumpOutput
    {
        return new DumpOutput($this);
    }

    public function dyn(callable $parserCallback): Dyn
    {
        return new Dyn($parserCallback, $this);
    }

    public function delimitedBy(
        ParserInterface|array|string $left,
        ParserInterface|array|string $right,
    ): Delimited {
        return new Delimited($left, $this, $right);
    }

    public function complete(): Complete
    {
        return new Complete($this);
    }

    public function followedBy(ParserInterface|array|string $parser): Followed
    {
        return new Followed($this, $parser);
    }

    public function ignored(): Ignored
    {
        return new Ignored($this);
    }

    public function leftSpaced(): LeftSpaced
    {
        return new LeftSpaced($this);
    }

    public function many0(): Many0
    {
        return new Many0($this);
    }

    public function many1(): Many1
    {
        return new Many1($this);
    }

    public function manyN(int $n): ManyN
    {
        return new ManyN($n, $this);
    }

    public function manyNM(int $n, int $m): ManyNM
    {
        return new ManyNM($n, $m, $this);
    }

    public function map(callable $mapper): Map
    {
        return new Map($this, $mapper);
    }

    public function mapParser(array|string|ParserInterface $parser): MapParser
    {
        return new MapParser($this, $parser);
    }

    public function mapRes(callable $mapper): MapRes
    {
        return new MapRes($this, $mapper);
    }

    public function opt(): Opt
    {
        return new Opt($this);
    }

    public function not(): Not
    {
        return new Not($this);
    }

    public function orFatal(?string $fatalContextMessage = null): OrFatal
    {
        return new OrFatal($this, $fatalContextMessage);
    }

    public function precededBy(ParserInterface|array|string $parser): Preceded
    {
        return new Preceded($parser, $this);
    }

    public function rightSpaced(): RightSpaced
    {
        return new RightSpaced($this);
    }

    public function separated0By(ParserInterface|array|string $separator): Separated0
    {
        return new Separated0($this, $separator);
    }

    public function separated1By(ParserInterface|array|string $separator): Separated1
    {
        return new Separated1($this, $separator);
    }

    public function spaced(): Spaced
    {
        return new Spaced($this);
    }

    public function spanned(): Spanned
    {
        return new Spanned($this);
    }

    public function toFloat(): ToFloat
    {
        return new ToFloat($this);
    }

    public function toInt(): ToInt
    {
        return new ToInt($this);
    }

    public function toString(): ToString
    {
        return new ToString($this);
    }

    public function trimmed(string $characters = " \n\r\t\v\0"): Trimmed
    {
        return new Trimmed($this, $characters);
    }

    public function value(mixed $value, bool $clone = false): Value
    {
        return new Value($this, $value, $clone);
    }

    /**
     * @param callable(mixed $token, ParseResult): bool $callback
     */
    public function verify(callable $callback): Verify
    {
        return new Verify($this, $callback);
    }

    public function withSubContext(mixed $subContext): WithSubContext
    {
        return new WithSubContext($subContext, $this);
    }
}
