<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream;

use Dajoha\ParserCombinator\Stream\Base\Span\SpanInterface;

/**
 * @implements SpanInterface<int>
 */
readonly class PositionSpan implements SpanInterface
{
    public function __construct(public int $start, public int $end)
    {
    }

    public function getStart(): int
    {
        return $this->start;
    }

    public function getEnd(): int
    {
        return $this->end;
    }
}
