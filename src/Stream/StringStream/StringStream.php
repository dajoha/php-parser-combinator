<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\StringStream;

use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\Base\TakeResult;

/**
 * @implements StreamInterface<StringPosition>
 */
readonly class StringStream implements StreamInterface
{
    public int $len;

    public function __construct(
        public string $input,
        public mixed $context = null,
        public int $index = 0,
        public int $charPosition = 0,
    ) {
        $this->len = strlen($this->input);
    }

    public function takeOne(): ?TakeResult
    {
        $firstByte = $this->input[$this->index] ?? null;
        if ($firstByte === null) {
            return null;
        }

        $charLength = self::getCharLength($firstByte);
        $newIndex = $this->index + $charLength;
        if ($newIndex > $this->len) {
            return null;
        }

        $newStream = new StringStream(
            $this->input,
            $this->context,
            $newIndex,
            $this->charPosition + 1,
        );

        $char = substr($this->input, $this->index, $charLength);

        return new TakeResult($newStream, $char);
    }

    public function getPosition(): StringPosition
    {
        return new StringPosition($this->index, $this->charPosition);
    }

    public function getSpanTo(StreamInterface $stream): StringSpan
    {
        return new StringSpan($this->getPosition(), $stream->getPosition());
    }

    public function isEof(): bool
    {
        return $this->index >= $this->len;
    }

    public function getContext(): mixed
    {
        return $this->context;
    }

    public function withContext(mixed $context): StreamInterface
    {
        return new StringStream($this->input, $context, $this->index, $this->charPosition);
    }

    protected static function getCharLength(string $firstByte): int
    {
        $firstByte = ord($firstByte);

        return match (true) {
            $firstByte < 128 => 1,
            $firstByte < 224 => 2,
            $firstByte < 240 => 3,
            default => 4,
        };
    }
}
