<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\StringStream;

readonly class StringPosition
{
    public function __construct(public int $index = 0, public int $charPosition = 0)
    {
    }
}
