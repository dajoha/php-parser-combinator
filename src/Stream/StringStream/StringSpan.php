<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\StringStream;

use Dajoha\ParserCombinator\Stream\Base\Span\SpanInterface;

/**
 * @implements SpanInterface<StringPosition>
 */
class StringSpan implements SpanInterface
{
    public function __construct(public StringPosition $start, public StringPosition $end)
    {
    }

    public function getStart(): StringPosition
    {
        return $this->start;
    }

    public function getEnd(): StringPosition
    {
        return $this->end;
    }
}
