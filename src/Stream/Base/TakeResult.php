<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\Base;

readonly class TakeResult
{
    public function __construct(
        public StreamInterface $stream,
        public mixed $token,
    ) {
    }
}
