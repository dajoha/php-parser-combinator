<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\Base;

use Dajoha\ParserCombinator\Stream\Base\Span\SpanInterface;

/**
 * @template P
 */
interface StreamInterface
{
    public function takeOne(): ?TakeResult;

    /**
     * @return P
     */
    public function getPosition(): mixed;

    /**
     * @return SpanInterface<P>
     */
    public function getSpanTo(StreamInterface $stream): SpanInterface;

    public function isEof(): bool;

    public function getContext(): mixed;

    public function withContext(mixed $context): self;
}
