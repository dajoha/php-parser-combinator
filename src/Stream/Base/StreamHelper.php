<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\Base;

use Dajoha\ParserCombinator\Stream\ArrayStream;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;

class StreamHelper
{
    public static function createStreamFrom(StreamInterface|string|array $input): StreamInterface
    {
        return match (true) {
            is_string($input) => new StringStream($input),
            is_array($input) => new ArrayStream($input),
            default => $input,
        };
    }
}
