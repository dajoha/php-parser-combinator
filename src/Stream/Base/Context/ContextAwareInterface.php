<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\Base\Context;

interface ContextAwareInterface
{
    public function setParseContext(mixed $parseContext);
}
