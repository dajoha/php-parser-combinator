<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\Base\Context;

trait ContextAwareTrait
{
    protected mixed $parseContext = null;
    protected bool $isParseContextInitialized = false;

    /**
     * Only define the context if it was not already initialized.
     */
    public function setParseContext(mixed $parseContext): static
    {
        if (!$this->isParseContextInitialized) {
            $this->parseContext = is_object($parseContext) ? clone $parseContext : $parseContext;
            $this->isParseContextInitialized = true;
        }

        return $this;
    }

    public function getParseContext(): mixed
    {
        return $this->parseContext;
    }
}
