<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\Base\Span;

/**
 * @template P
 */
interface SpanInterface
{
    /**
     * @return P
     */
    public function getStart();

    /**
     * @return P
     */
    public function getEnd();
}
