<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\Base\Span;

interface SpanAwareInterface
{
    public function setSpan(SpanInterface $span): static;
}
