<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\Base\Span;

trait SpanAwareTrait
{
    protected ?SpanInterface $span = null;

    /**
     * Only define the span if it was not already defined.
     */
    public function setSpan(SpanInterface $span): static
    {
        $this->span ??= $span;

        return $this;
    }

    public function getSpan(): ?SpanInterface
    {
        return $this->span;
    }
}
