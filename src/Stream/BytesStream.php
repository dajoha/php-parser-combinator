<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream;

use Dajoha\ParserCombinator\Stream\Base\Span\SpanInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\Base\TakeResult;

/**
 * @implements StreamInterface<int>
 */
readonly class BytesStream implements StreamInterface
{
    public int $len;

    public function __construct(
        public string $input,
        public mixed $context = null,
        public int $index = 0,
    ) {
        $this->len = strlen($this->input);
    }

    public function takeOne(): ?TakeResult
    {
        $byte = $this->input[$this->index] ?? null;
        if ($byte === null) {
            return null;
        }

        $newStream = new BytesStream($this->input, $this->context, $this->index + 1);

        return new TakeResult($newStream, $byte);
    }

    public function getPosition(): int
    {
        return $this->index;
    }

    public function getSpanTo(StreamInterface $stream): SpanInterface
    {
        return new PositionSpan($this->getPosition(), $stream->getPosition());
    }

    public function isEof(): bool
    {
        return $this->index >= $this->len;
    }

    public function getContext(): mixed
    {
        return $this->context;
    }

    public function withContext(mixed $context): StreamInterface
    {
        return new BytesStream($this->input, $context, $this->index);
    }
}
