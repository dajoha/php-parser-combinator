<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\LocatedStream;

/**
 * @template BP
 */
readonly class LocatedPosition
{
    public function __construct(
        /** @var BP $base */
        public mixed $base,
        public int $line,
        public int $column,
    ) {
    }
}
