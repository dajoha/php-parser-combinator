<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\LocatedStream;

use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\Base\TakeResult;

/**
 * @implements StreamInterface<LocatedPosition>
 */
readonly class LocatedStream implements StreamInterface
{
    public StreamInterface $innerStream;
    public int $line;
    public int $column;

    public function __construct(
        StreamInterface|string|array $innerStream,
        public int $baseLine = 1,
        public int $baseColumn = 1,
        ?int $line = null,
        ?int $column = null,
    ) {
        $this->innerStream = StreamHelper::createStreamFrom($innerStream);
        $this->line = $line ?? $this->baseLine;
        $this->column = $column ?? $this->baseColumn;
    }

    public function takeOne(): ?TakeResult
    {
        $takeResult = $this->innerStream->takeOne();
        if ($takeResult === null) {
            return null;
        }

        $innerStream = $takeResult->stream;
        $token = $takeResult->token;
        $isLineReturn = false;

        if ($token === "\n") {
            $isLineReturn = true;
        } elseif ($token === "\r") {
            $isLineReturn = true;
            $takeResult2 = $takeResult->stream->takeOne();
            if ($takeResult2?->token === "\n") {
                $innerStream = $takeResult2->stream;
                $token = "\r\n";
            }
        }

        if ($isLineReturn) {
            $line = $this->line + 1;
            $column = $this->baseColumn;
        } else {
            $line = $this->line;
            $column = $this->column + 1;
        }

        $locatedStream = new LocatedStream(
            $innerStream,
            $this->baseLine,
            $this->baseColumn,
            $line,
            $column,
        );

        return new TakeResult($locatedStream, $token);
    }

    public function getPosition(): LocatedPosition
    {
        return new LocatedPosition($this->innerStream->getPosition(), $this->line, $this->column);
    }

    public function getSpanTo(StreamInterface $stream): LocatedSpan
    {
        return new LocatedSpan($this->getPosition(), $stream->getPosition());
    }

    public function isEof(): bool
    {
        return $this->innerStream->isEof();
    }

    public function getContext(): mixed
    {
        return $this->innerStream->getContext();
    }

    public function withContext(mixed $context): StreamInterface
    {
        return new LocatedStream(
            $this->innerStream->withContext($context),
            $this->baseLine,
            $this->baseColumn,
            $this->line,
            $this->column,
        );
    }
}
