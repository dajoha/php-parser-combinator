<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Stream\LocatedStream;

use Dajoha\ParserCombinator\Stream\Base\Span\SpanInterface;

/**
 * @implements SpanInterface<LocatedPosition>
 */
readonly class LocatedSpan implements SpanInterface
{
    public function __construct(public LocatedPosition $start, public LocatedPosition $end)
    {
    }

    public function getStart(): LocatedPosition
    {
        return $this->start;
    }

    public function getEnd(): LocatedPosition
    {
        return $this->end;
    }
}
