<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Helper;

use Dajoha\ParserCombinator\Base\ParserInterface;
use ReflectionEnum;
use UnitEnum;

class DescriptionHelper
{
    public static function stringifyMixed(mixed $value): string
    {
        if ($value === true) return "true";
        if ($value === false) return "false";
        if ($value === null) return "null";

        if (is_string($value)) {
            $value = str_replace(["\t", "\r", "\n"], ["\\t", "\\r", "\\n"], $value);
            return $value === '"' ? "'\"'" : "\"$value\"";
        }

        if (is_numeric($value)) {
            return (string) $value;
        }

        if ($value instanceof UnitEnum) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $enumName = explode('\\', (new ReflectionEnum($value))->getName());
            $enumName = $enumName[count($enumName) - 1];
            $valueName = $value->name;
            return "$enumName::$valueName";
        }

        if (is_object($value)) {
            return "(object)";
        }

        return "(?)";
    }

    public static function indentMultiline(string $description, int|string $spaces): string
    {
        $spaces = is_integer($spaces) ? str_repeat(' ', $spaces) : $spaces;

        return str_replace("\n", "\n$spaces", $description);
    }

    public static function readableImplode(array $strings, string $lastJoin): string
    {
        if (empty($strings)) {
            return '';
        }

        $parts = [$strings[0]];
        for ($i = 1; $i < count($strings); $i++) {
            $parts[] = ', ';
            $parts[] = $strings[$i];
        }

        if (count($strings) > 1) {
            $parts[count($parts) - 2] = $lastJoin;
        }

        return implode($parts);
    }

    /**
     * @param ParserInterface[]                        $parsers
     * @param string|callable(ParserInterface): string $prefix
     *
     * @noinspection PhpDocSignatureIsNotCompleteInspection
     */
    public static function multiline(
        array $parsers,
        string $messageStart,
        string $lastJoin,
        string|callable $prefix = ' - ',
    ): string
    {
        $parsers = array_values($parsers);
        $parsersDescriptionsLc = array_map(
            fn($parser) => $parser->getDescriptionLc(),
            $parsers,
        );
        $descriptionsImploded = implode($parsersDescriptionsLc);

        $multiline = str_contains($descriptionsImploded, "\n") || strlen($descriptionsImploded) > 40;

        if ($multiline) {
            $parsersDescriptions = array_map(
                fn($description) => ucfirst($description),
                $parsersDescriptionsLc,
            );

            $description = "$messageStart:\n";
            foreach ($parsersDescriptions as $i => $parserDescription) {
                $currentPrefix = is_callable($prefix) ? $prefix($parsers[$i]) : $prefix;
                $parserDescription = rtrim(DescriptionHelper::indentMultiline($parserDescription, 3));
                $description .= $currentPrefix . $parserDescription . "\n";
            }
        } else {
            $description = "$messageStart " . DescriptionHelper::readableImplode($parsersDescriptionsLc, $lastJoin);
        }

        return $description;
    }

    public static function ignored(string $description): string
    {
        $description = lcfirst($description);

        return "(Ignored: $description)";
    }
}
