<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Helper;

class Predicate
{
    /**
     * @return callable(string $c): bool
     */
    public static function isDigit(): callable
    {
        return fn(string $c) => '0' <= $c && $c <= '9';
    }

    /**
     * @return callable(string $c): bool
     */
    public static function isAlpha(): callable
    {
        return fn(string $c) =>
            'a' <= $c && $c <= 'z'
            || 'A' <= $c && $c <= 'Z'
        ;
    }

    /**
     * @return callable(string $c): bool
     */
    public static function isAlphanumeric(): callable
    {
        return fn(string $c) =>
            'a' <= $c && $c <= 'z'
            || 'A' <= $c && $c <= 'Z'
            || '0' <= $c && $c <= '9'
        ;
    }

    /**
     * @return callable(string $c): bool
     */
    public static function isLower(): callable
    {
        return fn(string $c) => 'a' <= $c && $c <= 'z';
    }

    /**
     * @return callable(string $c): bool
     */
    public static function isUpper(): callable
    {
        return fn(string $c) => 'A' <= $c && $c <= 'Z';
    }

    /**
     * @return callable(string $c): bool
     */
    public static function isHSpace(): callable
    {
        return fn(string $c) => $c === ' ' || $c === "\t";
    }

    /**
     * @return callable(string $c): bool
     */
    public static function isVSpace(): callable
    {
        return fn(string $c) => $c === "\n" || $c === "\r" || $c === "\r\n";
    }

    /**
     * @return callable(string $c): bool
     */
    public static function isSpace(): callable
    {
        return fn(string $c) => $c === ' ' || $c === "\t" || $c === "\n" || $c === "\r" || $c === "\r\n";
    }

    /**
     * @return callable(string $c): bool
     */
    public static function isPrintable(): callable
    {
        return fn(string $c) => ord($c) >= 32;
    }

    /**
     * @return callable(string $c): bool
     */
    public static function is(string $expectedChar): callable
    {
        return fn(string $c) => $c === $expectedChar;
    }

    /**
     * @param (callable(string $c): bool)|string $predicate
     * @return callable(string $c): bool
     */
    public static function not(callable|string $predicate): callable
    {
        $predicate = self::toPredicate($predicate);

        return fn($c) => !$predicate($c);
    }

    /**
     * @param (callable(string $c): bool)|string ...$predicates
     * @return callable(string $c): bool
     */
    public static function all(callable|string ...$predicates): callable
    {
        $predicates = array_map(self::toPredicate(...), $predicates);

        return function(string $c) use ($predicates) {
            foreach ($predicates as $predicate) {
                if (!$predicate($c)) {
                    return false;
                }
            }

            return true;
        };
    }

    /**
     * @param (callable(string $c): bool)|string ...$predicates
     * @return callable(string $c): bool
     */
    public static function any(callable|string ...$predicates): callable
    {
        $predicates = array_map(self::toPredicate(...), $predicates);

        return function(string $c) use ($predicates) {
            foreach ($predicates as $predicate) {
                if ($predicate($c)) {
                    return true;
                }
            }

            return false;
        };
    }

    /**
     * @param (callable(string $c): bool)|string $value
     * @return callable(string $c): bool
     */
    protected static function toPredicate(string|callable $value): callable
    {
        return is_string($value) ? self::is($value) : $value;
    }
}
