<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Expression\Ast;

readonly class UnaryOperation
{
    public function __construct(
        public string $operator,
        public mixed $value,
    ) {
    }
}
