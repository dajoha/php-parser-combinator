<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Expression\Ast;

readonly class BinaryOperation
{
    public function __construct(
        public string $operator,
        public mixed $lhs,
        public mixed $rhs,
    ) {
    }
}
