<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Expression;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Expression\Ast\BinaryOperation;
use Dajoha\ParserCombinator\Expression\Ast\UnaryOperation;
use Dajoha\ParserCombinator\Parser\Misc\InnerParser;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Misc\descriptionLimiter;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;

class ExpressionParser extends InnerParser
{
    /**
     * @param ParserInterface $highestPrecedenceParser
     * @param array<callable(ParserInterface $higherPrecParser): ParserInterface> $parserProviders
     * @return ParserInterface
     */
    public static function precedenceParser(
        ParserInterface $highestPrecedenceParser,
        array $parserProviders,
    ): ParserInterface
    {
        $parser = $highestPrecedenceParser;

        foreach ($parserProviders as $parserProvider) {
            $parser = $parserProvider($parser);
        }

        return $parser;
    }

    public static function prefixOper(
        ParserInterface $operatorParser,
        ?callable $outputConstructor = null,
    ): callable
    {
        return function (ParserInterface $higherPrecedenceParser) use ($operatorParser, $outputConstructor) {
            return self::prefixOperParser($higherPrecedenceParser, $operatorParser, $outputConstructor);
        };
    }

    public static function suffixOper(
        ParserInterface $operatorParser,
        ?callable $outputConstructor = null,
    ): callable
    {
        return function (ParserInterface $higherPrecedenceParser) use ($operatorParser, $outputConstructor) {
            return self::suffixOperParser($higherPrecedenceParser, $operatorParser, $outputConstructor);
        };
    }

    public static function leftAssocBinaryOper(
        ParserInterface $operatorParser,
        ?callable $outputConstructor = null,
    ): callable
    {
        return function (ParserInterface $higherPrecedenceParser) use ($operatorParser, $outputConstructor) {
            return self::leftAssocBinaryOperParser($higherPrecedenceParser, $operatorParser, $outputConstructor);
        };
    }

    public static function rightAssocBinaryOper(
        ParserInterface $operatorParser,
        ?callable $outputConstructor = null,
    ): callable
    {
        return function (ParserInterface $higherPrecedenceParser) use ($operatorParser, $outputConstructor) {
            return self::rightAssocBinaryOperParser($higherPrecedenceParser, $operatorParser, $outputConstructor);
        };
    }

    /**
     * @param null|callable(mixed $operator, mixed $value, ParseResult $parseResult): mixed $outputConstructor
     *
     * @noinspection PhpDocSignatureIsNotCompleteInspection
     */
    public static function prefixOperParser(
        ParserInterface $higherPrecedenceParser,
        ParserInterface $operatorParser,
        ?callable $outputConstructor = null,
    ): ParserInterface
    {
        $outputConstructor ??= self::newUnaryOperation(...);

        return descriptionLimiter(alt(
            seq(
                many1($operatorParser),
                $higherPrecedenceParser,
            )->mapRes(function (ParseResult $parseResult) use ($outputConstructor) {
                $operators = $parseResult->output[0];
                $currentValue = $parseResult->output[1];
                for ($i = count($operators) - 1; $i >= 0; $i--) {
                    $operator = $operators[$i];
                    $currentValue = $outputConstructor($operator, $currentValue, $parseResult);
                }

                return $parseResult->withOutput($currentValue);
            }),
            $higherPrecedenceParser,
        ));
    }

    /**
     * @param null|callable(mixed $operator, mixed $value, ParseResult $parseResult): mixed $outputConstructor
     *
     * @noinspection PhpDocSignatureIsNotCompleteInspection
     */
    public static function suffixOperParser(
        ParserInterface $higherPrecedenceParser,
        ParserInterface $operatorParser,
        ?callable $outputConstructor = null,
    ): ParserInterface
    {
        $outputConstructor ??= self::newUnaryOperation(...);

        return descriptionLimiter(alt(
            seq(
                $higherPrecedenceParser,
                many1($operatorParser),
            )->mapRes(function (ParseResult $parseResult) use ($outputConstructor) {
                $currentValue = $parseResult->output[0];
                $operators = $parseResult->output[1];
                for ($i = 0; $i != count($operators); $i++) {
                    $operator = $operators[$i];
                    $currentValue = $outputConstructor($operator, $currentValue, $parseResult);
                }

                return $parseResult->withOutput($currentValue);
            }),
            $higherPrecedenceParser,
        ));
    }

    /**
     * @param null|callable(mixed $operator, mixed $lhs, mixed $rhs, ParseResult $parseResult): mixed $outputConstructor
     *
     * @noinspection PhpDocSignatureIsNotCompleteInspection
     */
    public static function leftAssocBinaryOperParser(
        ParserInterface $higherPrecedenceParser,
        ParserInterface $operatorParser,
        ?callable $outputConstructor = null,
    ): ParserInterface
    {
        $outputConstructor ??= self::newBinaryOperation(...);

        return descriptionLimiter(
            self::operationSequence($higherPrecedenceParser, $operatorParser)
                ->mapRes(function (ParseResult $parseResult) use ($outputConstructor) {
                    $tokens = $parseResult->output;
                    $currentLhs = array_shift($tokens);

                    for ($i = 0; $i + 1 < count($tokens); $i += 2) {
                        [$operator, $rhs] = [ $tokens[$i], $tokens[$i+1] ];
                        $currentLhs = $outputConstructor($operator, $currentLhs, $rhs, $parseResult);
                    }

                    return $parseResult->withOutput($currentLhs);
                }),
        );
    }

    /**
     * @param null|callable(mixed $operator, mixed $lhs, mixed $rhs, ParseResult $parseResult): mixed $outputConstructor
     *
     * @noinspection PhpDocSignatureIsNotCompleteInspection
     */
    public static function rightAssocBinaryOperParser(
        ParserInterface $higherPrecedenceParser,
        ParserInterface $operatorParser,
        ?callable $outputConstructor = null,
    ): ParserInterface
    {
        $outputConstructor ??= self::newBinaryOperation(...);

        return descriptionLimiter(
            self::operationSequence($higherPrecedenceParser, $operatorParser)
                ->mapRes(function (ParseResult $parseResult) use ($outputConstructor) {
                    $tokens = $parseResult->output;
                    $currentRhs = array_pop($tokens);

                    for ($i = count($tokens) - 2; $i >= 0; $i -= 2) {
                        [$lhs, $operator] = [ $tokens[$i], $tokens[$i+1] ];
                        $currentRhs = $outputConstructor($operator, $lhs, $currentRhs, $parseResult);
                    }

                    return $parseResult->withOutput($currentRhs);
                }),
        );
    }

    public static function operationSequence(
        ParserInterface $higherPrecedenceParser,
        ParserInterface $operatorParser,
    ): ParserInterface
    {
        return
            seq(
                $higherPrecedenceParser,
                many0(seq($operatorParser, $higherPrecedenceParser)),
            )
            ->map(fn(array $output) => array_merge([$output[0]], ...$output[1]))
        ;
    }

    /**
     * @noinspection PhpUnusedParameterInspection
     */
    protected static function newUnaryOperation(
        string $operator,
        mixed $value,
        ParseResult $parseResult,
    ): UnaryOperation
    {
        return new UnaryOperation($operator, $value);
    }

    /**
     * @noinspection PhpUnusedParameterInspection
     */
    protected static function newBinaryOperation(
        string $operator,
        mixed $lhs,
        mixed $rhs,
        ParseResult $parseResult,
    ): BinaryOperation
    {
        return new BinaryOperation($operator, $lhs, $rhs);
    }
}
