set positional-arguments
set ignore-comments

alias t := test

# Run the test suite
[no-exit-message]
@test *phpunit_args:
    ./vendor/bin/phpunit "$@"
