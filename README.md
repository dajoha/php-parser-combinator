# dajoha/parser-combinator

Yet another parser combinator for Php.

## Installing

In your php project, run

```
composer require dajoha/parser-combinator
```

## Basic usage

```php
<?php

use function Dajoha\ParserCombinator\Multi\separated0;
use function Dajoha\ParserCombinator\String\integer;
use function Dajoha\ParserCombinator\String\spaced;

$parser = separated0(integer(), spaced(','));
$input = '12, 45';
var_dump($parser->parse($input)->output);
```

#### Output:

```
array(2) {
  [0]=>
  int(12)
  [1]=>
  int(45)
}
```
