<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Character;

use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\Character\Alpha;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\alpha;

#[CoversClass(Alpha::class)]
class AlphaTest extends TestCase
{
    #[TestDox('Parsing alphabetic char in "$input" should be $expectedStatus')]
    #[DataProvider('dataProvider')]
    public function testAlpha(string $input, ParseStatus $expectedStatus)
    {
        $parseResult = alpha()->parse($input);
        $this->assertSame($expectedStatus, $parseResult->status);
    }

    public static function dataProvider(): array
    {
        return [
            ['', ParseStatus::Error],
            ['4', ParseStatus::Error],
            ['-', ParseStatus::Error],
            ['é', ParseStatus::Error],
            ['a', ParseStatus::Success],
            ['A', ParseStatus::Success],
            ['r', ParseStatus::Success],
        ];
    }
}
