<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Character;

use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\Character\Digit;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;

#[CoversClass(Digit::class)]
class DigitTest extends TestCase
{
    #[TestDox('Parsing digit in "$input" should be $expectedStatus')]
    #[DataProvider('dataProvider')]
    public function testDigit(string $input, ParseStatus $expectedStatus)
    {
        $parseResult = digit()->parse($input);
        $this->assertSame($expectedStatus, $parseResult->status);
    }

    public static function dataProvider(): array
    {
        return [
            ['', ParseStatus::Error],
            ['a', ParseStatus::Error],
            ['-', ParseStatus::Error],
            ['é', ParseStatus::Error],
            ['0', ParseStatus::Success],
            ['8', ParseStatus::Success],
            ['9', ParseStatus::Success],
        ];
    }
}
