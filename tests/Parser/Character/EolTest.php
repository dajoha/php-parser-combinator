<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Character;

use Dajoha\ParserCombinator\Parser\Character\Eol;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\eol;

#[CoversClass(Eol::class)]
class EolTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testParsingEndOfLine(string $input, string $expected): void
    {
        $this->assertSame($expected, eol()->parse($input)->output);
    }

    #[DataProvider('failingDataProvider')]
    public function testParsingEndOfLineFails(string $input): void
    {
        $this->assertTrue(eol()->parse($input)->isError());
    }

    public static function dataProvider(): array
    {
        return [
            ["\n", "\n"],
            ["\r", "\r"],
            ["\r!!", "\r"],
            ["\r\n", "\r\n"],
            ["\r\n!!", "\r\n"],
        ];
    }

    public static function failingDataProvider(): array
    {
        return [
            [''],
            ['1'],
            ['..'],
        ];
    }
}
