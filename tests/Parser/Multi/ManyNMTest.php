<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Multi;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Multi\ManyNM;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Multi\manyNM;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(ManyNM::class)]
class ManyNMTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testManyNM(
        string|array $input,
        ParserInterface $parser,
        ?array $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertEquals($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testManyNMDescription()
    {
        $parser = any()->manyNM(3, 5);
        $this->assertSame("Between 3 and 5 of any token", $parser->getDescription());
    }

    public static function dataProvider(): array {
        return [
            ['', manyNM(0, 0, '@'), [], 0],
            ['@@@', manyNM(0, 0, '@'), [], 0],
            ['@@@', manyNM(0, 1, '@'), ['@'], 1],
            ['', manyNM(1, 1, '@'), null, 0],
            ['abc', manyNM(1, 1, '@'), null, 0],
            ['', manyNM(3, 3, '@'), null, 0],
            ['abc', manyNM(3, 3, '@'), null, 0],
            ['@', manyNM(1, 1,'@'), ['@'], 1],
            ['@', manyNM(1, 2,'@'), ['@'], 1],
            ['@!!', manyNM(1, 2,'@'), ['@'], 1],
            ['@@@', manyNM(1, 1, '@'), ['@'], 1],
            ['@@@', manyNM(1, 2, '@'), ['@', '@'], 2],
            ['@', manyNM(2, 2, '@'), null, 1],
            ['@@', manyNM(2, 2, '@'), ['@', '@'], 2],
            ['@@@', manyNM(2, 2, '@'), ['@', '@'], 2],
            ['@@@', manyNM(2, 3, '@'), ['@', '@', '@'], 3],
            ['@@@!!', manyNM(2, 3, '@'), ['@', '@', '@'], 3],
        ];
    }

}
