<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Multi;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Multi\Many1;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(Many1::class)]
class Many1Test extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testMany1(
        string|array $input,
        ParserInterface $parser,
        ?array $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertEquals($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testMany1Description()
    {
        $parser = any()->many1();
        $this->assertSame("1-or-more of any token", $parser->getDescription());
    }

    public static function dataProvider(): array {
        return [
            ['', many1('@'), null, 0],
            ['!@@', many1('@'), null, 0],
            ['@', many1('@'), ['@'], 1],
            ['@@', many1('@'), ['@', '@'], 2],
            ['@@!', many1('@'), ['@', '@'], 2],
            ['', many1(digit()), null, 0],
            ['1', many1(digit()), ['1'], 1],
            ['147!', many1(digit()), ['1', '4', '7'], 3],
            [
                [
                    'a',
                    new IgnoredOutput('b'),
                    'c',
                    '!'
                ],
                many1(any()->verify(fn($t) => $t !== '!')),
                ['a', 'c'],
                3,
            ],
            [
                [
                    'a',
                    new IgnoredOutput('b'),
                    'c',
                    '!'
                ],
                many1(any()->verify(fn($t) => $t !== '!'))->skipIgnored(false),
                [
                    'a',
                    new IgnoredOutput('b'),
                    'c',
                ],
                3,
            ],
        ];
    }
}
