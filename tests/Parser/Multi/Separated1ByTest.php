<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Multi;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Multi\Separated1;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Multi\separated1;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\Token\any;
use function Dajoha\ParserCombinator\Parser\Token\one;

#[CoversClass(Separated1::class)]
class Separated1ByTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testSeparated1By(
        string|array $input,
        ParserInterface $parser,
        ?array $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertEquals($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testSeparated0ByDescription()
    {
        $parser = separated1(integer(), ",");
        $this->assertSame("1-or-more of one integer separated by \",\"", $parser->getDescription());
    }

    public static function dataProvider(): array {
        return [
            ['', separated1(integer(), '/'), null, 0],
            ['abc', separated1(integer(), '/'), null, 0],
            ['///', separated1(integer(), '/'), null, 0],
            ['78!', separated1(integer(), '/'), [78], 2],
            ['78/!', separated1(integer(), '/'), [78], 2],
            ['78//123!', separated1(integer(), '/'), [78], 2],
            ['78/13!', separated1(integer(), '/'), [78, 13], 5],
            ['78/13/123/15!', separated1(integer(), '/'), [78, 13, 123, 15], 12],
            [
                [
                    'a',
                    '====',
                    new IgnoredOutput('b'),
                    '====',
                    'c',
                    '!'
                ],
                any()->separated0By(one('====')),
                ['a', 'c'],
                5,
            ],
            [
                [
                    'a',
                    '====',
                    new IgnoredOutput('b'),
                    '====',
                    'c',
                    '!'
                ],
                any()->separated0By(one('===='))->skipIgnored(false),
                [
                    'a',
                    new IgnoredOutput('b'),
                    'c',
                ],
                5,
            ],
        ];
    }
}
