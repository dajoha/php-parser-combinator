<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Multi;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Multi\Many0;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(Many0::class)]
class Many0Test extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testMany0(
        string|array $input,
        ParserInterface $parser,
        ?array $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertEquals($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testMany0Description()
    {
        $parser = any()->many0();
        $this->assertSame("0-or-more of any token", $parser->getDescription());
    }

    public static function dataProvider(): array {
        return [
            ['', many0('@'), [], 0],
            ['!@@', many0('@'), [], 0],
            ['@', many0('@'), ['@'], 1],
            ['@@', many0('@'), ['@', '@'], 2],
            ['@@!', many0('@'), ['@', '@'], 2],
            ['', many0(digit()), [], 0],
            ['1', many0(digit()), ['1'], 1],
            ['147!', many0(digit()), ['1', '4', '7'], 3],
            [
                [
                    'a',
                    new IgnoredOutput('b'),
                    'c',
                    '!'
                ],
                many0(any()->verify(fn($t) => $t !== '!')),
                ['a', 'c'],
                3,
            ],
            [
                [
                    'a',
                    new IgnoredOutput('b'),
                    'c',
                    '!'
                ],
                many0(any()->verify(fn($t) => $t !== '!'))->skipIgnored(false),
                [
                    'a',
                    new IgnoredOutput('b'),
                    'c',
                ],
                3,
            ],
        ];
    }
}
