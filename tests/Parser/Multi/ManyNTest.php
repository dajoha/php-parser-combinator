<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Multi;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Multi\ManyN;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Multi\manyN;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(ManyN::class)]
class ManyNTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testManyN(
        string|array $input,
        ParserInterface $parser,
        ?array $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertEquals($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testManyNDescription()
    {
        $parser = any()->manyN(3);
        $this->assertSame("3 of any token", $parser->getDescription());
    }

    public static function dataProvider(): array {
        return [
            ['', manyN(0, '@'), [], 0],
            ['@@@', manyN(0, '@'), [], 0],
            ['', manyN(1, '@'), null, 0],
            ['abc', manyN(1, '@'), null, 0],
            ['', manyN(3, '@'), null, 0],
            ['abc', manyN(3, '@'), null, 0],
            ['@', manyN(1, '@'), ['@'], 1],
            ['@@@', manyN(1, '@'), ['@'], 1],
            ['@@', manyN(2, '@'), ['@', '@'], 2],
            ['@@@@', manyN(2, '@'), ['@', '@'], 2],
            ['@@!', manyN(2, '@'), ['@', '@'], 2],
            [
                [ 'a', new IgnoredOutput('b'), 'c', '!' ],
                manyN(1, any()->verify(fn($t) => $t !== '!')),
                ['a'],
                1,
            ],
            [
                [ 'a', new IgnoredOutput('b'), 'c', '!' ],
                manyN(2, any()->verify(fn($t) => $t !== '!')),
                ['a'],
                2,
            ],
            [
                [ 'a', new IgnoredOutput('b'), 'c', '!' ],
                manyN(3, any()->verify(fn($t) => $t !== '!')),
                ['a', 'c'],
                3,
            ],
            [
                [ 'a', new IgnoredOutput('b'), 'c', '!' ],
                manyN(4, any()->verify(fn($t) => $t !== '!')),
                null,
                4,
            ],
        ];
    }
}
