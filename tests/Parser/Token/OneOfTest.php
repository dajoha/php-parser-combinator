<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Token;

use Dajoha\ParserCombinator\Parser\Token\OneOf;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Token\oneOf;

#[CoversClass(OneOf::class)]
class OneOfTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testOneOf(
        StreamInterface|string|array $input,
        string|array $oneOfTokens,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = oneOf($oneOfTokens)->parse($input);

        $this->assertSame($parseResult->output, $expectedOutput);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testOneOfDescription()
    {
        $parser = oneOf('abc');
        $this->assertSame("One of [\"a\", \"b\", \"c\"]", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', '', null, 0],
            ['', [], null, 0],
            [[], '', null, 0],
            [[], [], null, 0],

            ['a', '', null, 0],
            ['a', [], null, 0],
            [['a'], '', null, 0],
            [['a'], [], null, 0],

            ['é', '', null, 0],
            ['é', [], null, 0],
            [['é'], '', null, 0],
            [['é'], [], null, 0],

            ['a', 'abc', 'a', 1],
            ['a', 'cba', 'a', 1],
            ['a', ['b', 'a'], 'a', 1],

            ['abc', 'abc', 'a', 1],
            ['abc', 'cba', 'a', 1],
            ['abc', ['b', 'a'], 'a', 1],
            [['aa', 'bb', 'cc'], ['bb', 'aa'], 'aa', 1],
        ];
    }
}
