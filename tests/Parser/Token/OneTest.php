<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Token;

use Dajoha\ParserCombinator\Parser\Token\One;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Token\one;

#[CoversClass(One::class)]
class OneTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testOne(
        StreamInterface|string|array $input,
        string $token,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = one($token)->parse($input);

        $this->assertSame($parseResult->output, $expectedOutput);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testOneDescription()
    {
        $parser = one('a');
        $this->assertSame("One token 'a'", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', '', null, 0],
            [[], '', null, 0],
            ['', 'a', null, 0],
            [[], 'a', null, 0],
            ['', 'é', null, 0],
            [[], 'é', null, 0],
            ['hi', 'Z', null, 1],
            [['h', 'i'], 'Z', null, 1],
            ['hi', 'h', 'h', 1],
            [['h', 'i'], 'h', 'h', 1],
            ['hi', 'é', null, 1],
            [['h', 'i'], 'é', null, 1],
            ['éi', 'é', 'é', 2],
            [['é', 'i'], 'é', 'é', 1],
        ];
    }
}
