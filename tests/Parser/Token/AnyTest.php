<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Token;

use Dajoha\ParserCombinator\Parser\Token\Any;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(Any::class)]
class AnyTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testAnyToken(
        StreamInterface|string|array $input,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = any()->parse($input);

        $this->assertSame($parseResult->output, $expectedOutput);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public static function dataProvider(): array
    {
        return [
            ['', null, 0],
            [[], null, 0],
            ['a', 'a', 1],
            ['é', 'é', strlen('é')],
            [[78], 78, 1],
            ['abc', 'a', 1],
            [[78, 33, 42], 78, 1],
        ];
    }
}
