<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Token;

use Dajoha\ParserCombinator\Parser\Token\NoneOf;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Token\noneOf;

#[CoversClass(NoneOf::class)]
class NoneOfTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testNoneOf(
        StreamInterface|string|array $input,
        string|array $noneOfTokens,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = noneOf($noneOfTokens)->parse($input);

        $this->assertSame($parseResult->output, $expectedOutput);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

     public function testNoneOfDescription()
    {
        $parser = noneOf('abc');
        $this->assertSame("None of [\"a\", \"b\", \"c\"]", $parser->getDescription());
    }

   public static function dataProvider(): array
    {
        return [
            ['', '', null, 0],
            ['', [], null, 0],
            [[], '', null, 0],
            [[], [], null, 0],
            ['a', '', 'a', 1],
            ['a', [], 'a', 1],
            ['é', '', 'é', strlen('é')],
            ['é', [], 'é', strlen('é')],
            [[78], '', 78, 1],
            [[78], [], 78, 1],
            ['abc', '', 'a', 1],
            ['abc', [], 'a', 1],
            [[78, '', 33, 42], '', 78, 1],
            [[78, '', 33, 42], [], 78, 1],

            ['', 'z', null, 0],
            ['', ['z'], null, 0],
            [[], 'z', null, 0],
            [[], ['z'], null, 0],
            ['a', 'z', 'a', 1],
            ['a', ['z'], 'a', 1],
            ['é', 'z', 'é', strlen('é')],
            ['é', ['z'], 'é', strlen('é')],

            ['abc', 'abc', null, 0],
            ['abc', 'cba', null, 0],
            ['abc', ['c', 'b', 'a'], null, 0],
            ['éàf', ['c', 'b', 'a'], 'é', strlen('é')],
        ];
    }
}
