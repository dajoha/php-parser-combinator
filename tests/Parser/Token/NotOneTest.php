<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Token;

use Dajoha\ParserCombinator\Parser\Token\NotOne;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Token\notOne;

#[CoversClass(NotOne::class)]
class NotOneTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testNoneOf(
        StreamInterface|string|array $input,
        string $token,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = notOne($token)->parse($input);

        $this->assertSame($parseResult->output, $expectedOutput);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testNotOneDescription()
    {
        $parser = notOne('a');
        $this->assertSame("Not one token 'a'", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', '', null, 0],
            [[], '', null, 0],
            ['', 'a', null, 0],
            [[], 'a', null, 0],
            ['hi', 'Z', 'h', 1],
            [['h', 'i'], 'Z', 'h', 1],
            ['hi', 'h', null, 0],
            [['h', 'i'], 'h', null, 0],
        ];
    }
}
