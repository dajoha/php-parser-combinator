<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Sequence;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Sequence\Delimited;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Sequence\delimited;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Delimited::class)]
class DelimitedTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testDelimited(
        string $input,
        ParserInterface $parser,
        ?string $expectedOutput,
        int $expectedResultIndex,
    )
    {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testDelimitedDescription()
    {
        $parser = delimited('(', integer(), ')');
        $this->assertSame('One integer delimited by "(" and ")"', $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', delimited('(', 'a', ')'), null, 0],
            ['(', delimited('(', 'a', ')'), null, 1],
            ['(a', delimited('(', 'a', ')'), null, 2],
            ['(a)', delimited('(', 'a', ')'), 'a', 3],
            ['(a)!', delimited('(', 'a', ')'), 'a', 3],
            ['(b)!', delimited('(', 'a', ')'), null, 1],
        ];
    }
}
