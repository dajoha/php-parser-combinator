<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Sequence;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Parser\Sequence\Seq;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Sequence\ignored;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Seq::class)]
class SeqTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testSeq(
        string $input,
        ParserInterface|array $parsers,
        ?array $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parser = $parsers instanceof ParserInterface ? $parsers : seq(...$parsers);
        $parseResult = $parser->parse($input);
        $this->assertEquals($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testSeqDescription()
    {
        $parser = seq(integer(), identifier());
        $this->assertSame("A sequence of one integer and one identifier", $parser->getDescription());
        $parser = seq(integer(), identifier(), identifier()->ignored(), identifier());
        $this->assertSame(
            <<< EOF
            A sequence of:
             0. One integer
             1. One identifier
             ~. (Ignored: one identifier)
             2. One identifier

            EOF,
            $parser->getDescription(),
        );
    }

    public static function dataProvider(): array
    {
        return [
            ['', [], [], 0],
            ['', ['a', 'b', 'c'], null, 0],
            ['a', ['a', 'b', 'c'], null, 1],
            ['ab', ['a', 'b', 'c'], null, 2],
            ['abd', ['a', 'b', 'c'], null, 2],
            ['abc', ['a', 'b', 'c'], ['a', 'b', 'c'], 3],
            ['abcd', ['a', 'b', 'c'], ['a', 'b', 'c'], 3],
            ['abcd', ['a', ignored('b'), 'c'], ['a', 'c'], 3],
            ['abcd', [ignored('a'), ignored('b'), ignored('c')], [], 3],
            [
                'abcd',
                seq('a', ignored('b'), 'c')->skipIgnored(false),
                [
                    'a',
                    new IgnoredOutput('b'),
                    'c',
                ],
                3,
            ],
        ];
    }
}
