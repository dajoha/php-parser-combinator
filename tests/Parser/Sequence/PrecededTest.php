<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Sequence;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Sequence\Preceded;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Sequence\preceded;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(Preceded::class)]
class PrecededTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testPrecededBy(
        string $input,
        ParserInterface $parser,
        ?string $expectedOutput,
        int $expectedResultIndex,
    )
    {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }


    public function testPrecededDescription()
    {
        $parser = preceded(any(), integer());
        $this->assertSame('One integer preceded by any token', $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', preceded('(', 'a'), null, 0],
            ['(', preceded('(', 'a'), null, 1],
            ['(a', preceded('(', 'a'), 'a', 2],
            ['(a!', preceded('(', 'a'), 'a', 2],
            ['(b!', preceded('(', 'a'), null, 1],
        ];
    }
}
