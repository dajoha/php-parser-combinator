<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Sequence;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Sequence\Followed;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Sequence\followed;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Followed::class)]
class FollowedTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testFollowed(
        string $input,
        ParserInterface $parser,
        ?string $expectedOutput,
        int $expectedResultIndex,
    )
    {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testFollowedDescription()
    {
        $parser = followed(integer(), '!');
        $this->assertSame('One integer followed by "!"', $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', followed('a', ')'), null, 0],
            ['a', followed('a', ')'), null, 1],
            ['a)', followed('a', ')'), 'a', 2],
            ['a)!', followed('a', ')'), 'a', 2],
            ['b)!', followed('a', ')'), null, 0],
        ];
    }
}
