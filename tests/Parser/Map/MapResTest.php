<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Map;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\Map\MapRes;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Map\mapRes;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\Token\oneOf;

#[CoversClass(MapRes::class)]
class MapResTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testMapRes(
        string $input,
        ParserInterface $parser,
        ParseStatus $expectedStatus,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        $this->assertSame($expectedStatus, $parseResult->status);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }


    public function testMapDescription()
    {
        $parser = oneOf('abc')->mapRes(fn($v) => $v);
        $this->assertSame("A result map of one of [\"a\", \"b\", \"c\"]", $parser->getDescription());
    }

    public static function dataProvider(): array {
        $outputMapper = fn($n) => $n ? $n * 2 : 777;
        $newResultMapper = function(ParseStatus $status, callable $outputMapper) {
            return fn(ParseResult $r) => new ParseResult(
                $status,
                $r->stream,
                $outputMapper($r->output),
            );
        };

        return [
            [
                '',
                mapRes(integer(), $newResultMapper(ParseStatus::Success, $outputMapper)),
                ParseStatus::Error,
                null,
                0,
            ],
            [
                'abc',
                mapRes(integer(), $newResultMapper(ParseStatus::Success, $outputMapper)),
                ParseStatus::Error,
                null,
                0,
            ],
            [
                '111',
                mapRes(integer(), $newResultMapper(ParseStatus::Success, $outputMapper)),
                ParseStatus::Success,
                222,
                3,
            ],
            [
                '0',
                mapRes(integer(), $newResultMapper(ParseStatus::Success, $outputMapper)),
                ParseStatus::Success,
                777,
                1,
            ],
            [
                '111',
                mapRes(integer(), $newResultMapper(ParseStatus::Error, $outputMapper)),
                ParseStatus::Error,
                null,
                3,
            ],
            [
                '111!!',
                mapRes(integer(), $newResultMapper(ParseStatus::Success, $outputMapper)),
                ParseStatus::Success,
                222,
                3,
            ],
        ];
    }
}
