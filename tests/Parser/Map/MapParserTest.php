<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Map;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Map\MapParser;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\Token\any;
use function Dajoha\ParserCombinator\Parser\Token\noneOf;

#[CoversClass(MapParser::class)]
class MapParserTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testMapParser(
        string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        int $expectedResultIndex,
        mixed $expectedContext,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
        $this->assertSame($expectedContext, $stream->context);
    }

    public function testMapParserDescription()
    {
        $parser = any()->many1()->mapParser(digit()->many1());
        $this->assertSame("1-or-more of any token, then parsing 1-or-more of one digit", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            [
                '123abc!',
                many1(noneOf('!'))->mapParser(integer()),
                123,
                6,
                null,
            ],
            [
                '123abc!',
                many1(noneOf('!'))
                    ->mapParser(
                        integer()->alterContext(fn() => 'hi')
                    )
                ,
                123,
                6,
                'hi',
            ],
        ];
    }
}
