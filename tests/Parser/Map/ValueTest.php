<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Map;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Map\Value;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Value::class)]
class ValueTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testValue(
        StreamInterface|string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testValueClone()
    {
        $obj = (object) ['foo' => 123];

        $parser = identifier()->value($obj);
        $parseResult = $parser->parse('blue sky');
        $this->assertSame(spl_object_id($parseResult->output), spl_object_id($obj));

        $parser = identifier()->value($obj, true);
        $parseResult = $parser->parse('blue sky');
        $this->assertNotSame(spl_object_id($parseResult->output), spl_object_id($obj));
    }

    public function testValueDescription()
    {
        $parser = integer()->value(true);
        $this->assertSame("One integer which maps to value true", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['blue sky', identifier()->value('BLUE'), 'BLUE', 4],
            ['123 sky', identifier()->value('BLUE'), null, 0],
        ];
    }
}
