<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Map;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Map\Map;
use Dajoha\ParserCombinator\Stream\Base\Context\ContextAwareInterface;
use Dajoha\ParserCombinator\Stream\Base\Context\ContextAwareTrait;
use Dajoha\ParserCombinator\Stream\Base\Span\SpanAwareInterface;
use Dajoha\ParserCombinator\Stream\Base\Span\SpanAwareTrait;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringPosition;
use Dajoha\ParserCombinator\Stream\StringStream\StringSpan;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Map\map;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\String\spaced;
use function Dajoha\ParserCombinator\Parser\Token\any;
use function Dajoha\ParserCombinator\Parser\Token\oneOf;

#[CoversClass(Map::class)]
class MapTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testMap(
        StreamInterface|string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testMapDescription()
    {
        $parser = oneOf('abc')->map(fn($v) => $v);
        $this->assertSame("A map of one of [\"a\", \"b\", \"c\"]", $parser->getDescription());
    }

    public function testMapSpanAwareTrait()
    {
        $parser = spaced(integer()->map(
            fn(int $n) => new class($n) implements SpanAwareInterface {
                use SpanAwareTrait;

                public readonly int $value;

                public function __construct(int $n)
                {
                    $this->value = $n;
                }

                public function getSpan(): ?StringSpan
                {
                    return $this->span;
                }
            }
        ));

        $input = "  123 ";

        $parseResult = $parser->parse($input);

        $this->assertEquals(
            new StringSpan(
                new StringPosition(2, 2),
                new StringPosition(5, 5),
            ),
            $parseResult->output->getSpan(),
        );
    }

    public function testMapContextAwareTrait()
    {
        $parser = spaced(
            integer()
                ->alterContext(fn() => 'hello')
                ->map(fn(int $n) => new class($n) implements ContextAwareInterface {
                    use ContextAwareTrait;

                    public readonly int $value;

                    public function __construct(int $n)
                    {
                        $this->value = $n;
                    }
                })
        );

        $input = "  123 ";

        $parseResult = $parser->parse($input);

        $this->assertEquals(
            'hello',
            $parseResult->output->getParseContext(),
        );
    }

    public static function dataProvider(): array {
        $double = fn($n) => $n * 2;

        return [
            ['', map(integer(), $double), null, 0],
            ['abc', map(integer(), $double), null, 0],
            ['111', map(integer(), $double), 222, 3],
            ['111!!', map(integer(), $double), 222, 3],
            [
                new StringStream('12!34', 'SINGLE'),
                many0(
                    any()
                        ->map(fn($c, ParseResult $r) => $r->stream->getContext() === 'SINGLE' ? $c : "$c$c")
                        ->alterContext(function(ParseResult $parseResult) {
                            return $parseResult->output === '!' ? 'DOUBLE' : $parseResult->getContext();
                        })
                )->toString(),
                '12!3344',
                5,
            ],
        ];
    }
}
