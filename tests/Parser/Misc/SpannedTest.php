<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Misc;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Character\Alpha;
use Dajoha\ParserCombinator\Parser\Character\Digit;
use Dajoha\ParserCombinator\Parser\Misc\OutputWrapper\SpannedValue;
use Dajoha\ParserCombinator\Parser\Misc\Spanned;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\LocatedStream\LocatedPosition;
use Dajoha\ParserCombinator\Stream\LocatedStream\LocatedStream;
use Dajoha\ParserCombinator\Stream\StringStream\StringPosition;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Misc\nothing;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Multi\separated0;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\spaces0;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(Spanned::class)]
class SpannedTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testSpanned(
        string|StreamInterface $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        mixed $expectedStartPosition,
        mixed $expectedEndPosition,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertTrue($parseResult->isSuccess());
        /** @var SpannedValue $spanValue */
        $spanValue = $parseResult->output;
        [$value, $span] = [$spanValue->value, $spanValue->span];

        $this->assertSame($expectedOutput, $value);
        $this->assertEquals($expectedStartPosition, $span->getStart());
        $this->assertEquals($expectedEndPosition, $span->getEnd());
    }

    public static function dataProvider(): array
    {
        return [
            [
                '',
                nothing()->spanned(),
                null,
                new StringPosition(0, 0),
                new StringPosition(0, 0),
            ], [
                '   hi   ',
                identifier()->spanned()->precededBy(spaces0()),
                'hi',
                new StringPosition(3, 3),
                new StringPosition(5, 5),
            ], [
                new LocatedStream(<<< EOF
                    Word1   Word2
                    --Word3
                         Word4
                    EOF
                ),
                separated0(
                    identifier()->spanned(),
                    many0(any()->verify(fn($c) => !self::isAlphanumeric($c)))
                )->map(fn(array $identifiers) => $identifiers[2]),
                'Word3',
                new LocatedPosition(new StringPosition(16, 16), 2, 3),
                new LocatedPosition(new StringPosition(21, 21), 2, 8),
            ],
        ];
    }

    protected static function isAlphanumeric(string $c): bool
    {
        return in_array($c, Alpha::CHARS) || in_array($c, Digit::DIGITS);
    }
}
