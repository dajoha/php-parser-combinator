<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Misc;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Misc\Complete;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Misc\complete;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Complete::class)]
class CompleteTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testComplete(
        string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testAlterContextDescription()
    {
        $parser = integer()->complete();
        $this->assertSame("One integer with no remaining input", $parser->getDescription());
    }

    public static function dataProvider(): array {
        return [
            ['', complete(identifier()), null, 0],
            ['123', complete(identifier()), null, 0],
            ['abc', complete(identifier()), 'abc', 3],
            ['abc!', complete(identifier()), null, 3],
        ];
    }
}
