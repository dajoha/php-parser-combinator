<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Context;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Context\AlterContext;
use Dajoha\ParserCombinator\Stream\Base\StreamHelper;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(AlterContext::class)]
class AlterContextTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testAlterContext(
        StreamInterface $input,
        mixed $initialContext,
        ParserInterface $parser,
        mixed $expectedFinalContext,
    ) {
        $stream = StreamHelper::createStreamFrom($input)->withContext($initialContext);
        $parseResult = $parser->parse($stream);
        $this->assertEquals($expectedFinalContext, $parseResult->stream->getContext());
    }

    public function testAlterContextDescription()
    {
        $parser = integer()->alterContext(fn() => null);
        $this->assertSame("One integer with altered context", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            [
                new StringStream('hello'),
                0,
                many0(any()->alterContext(fn(ParseResult $r) => $r->stream->getContext() + 10)),
                50,
            ],
            [
                new StringStream('hello'),
                0,
                many0(any()->alterContext(function (ParseResult $r) {
                    return $r->stream->getContext() + ($r->output === 'e' ? 5 : 10);
                })),
                45,
            ],
        ];
    }
}
