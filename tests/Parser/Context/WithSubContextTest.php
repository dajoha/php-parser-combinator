<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Context;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Context\WithSubContext;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Context\withSubContext;
use function Dajoha\ParserCombinator\Parser\Dynamic\dynParser;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\Sequence\ignored;
use function Dajoha\ParserCombinator\Parser\String\str;

#[CoversClass(WithSubContext::class)]
class WithSubContextTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testWithSubContext(
        string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
    ) {
        $parseResult = $parser->parse($input);
        if ($expectedOutput === null) {
            $this->assertFalse($parseResult->isSuccess());
        } else {
            $this->assertEquals($expectedOutput, $parseResult->output);
        }
    }

    public static function dataProvider(): array
    {
        $numberWithGrowingDigitsParser = withSubContext(
            -1,
            many1(
                digit()
                    ->verify(function (string $digit, ParseResult $result) {
                        return (int) $digit > $result->getContext();
                    })
                    ->alterContext(function(ParseResult $result) {
                        return (int) $result->output;
                    })
            )->toInt(),
        );

        $strangeParser = withSubContext(
            false,
            many0(
                alt(
                    dynParser(function(StreamInterface $stream) use ($numberWithGrowingDigitsParser) {
                        if (true === $stream->getContext()) {
                            return $numberWithGrowingDigitsParser->map(fn($n) => $n * 2);
                        } else {
                            return $numberWithGrowingDigitsParser;
                        }
                    }),
                    ignored(str('!'))->alterContext(fn(ParseResult $r) => !$r->getContext())
                )
            ),
        );

        return [
            [ '', $numberWithGrowingDigitsParser, null ],
            [ '123', $numberWithGrowingDigitsParser, 123 ],
            [ '1123', $numberWithGrowingDigitsParser, 1 ],
            [ '321', $numberWithGrowingDigitsParser, 3 ],
            [ '343', $numberWithGrowingDigitsParser, 34 ],
            [
                '14768912abc',
                many0($numberWithGrowingDigitsParser),
                [147, 689, 12],
            ],
            [
                '123!123',
                $strangeParser,
                [123, 246],
            ],
            [
                '123123!123123',
                $strangeParser,
                [123, 123, 246, 246],
            ],
            [
                '454!14521!5434',
                $strangeParser,
                [45, 4, 290, 4, 2, 5, 4, 34],
            ],
        ];
    }
}
