<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Dynamic;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Dynamic\Dyn;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Dynamic\dyn;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Dyn::class)]
class DynTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testDyn(
        StreamInterface|string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testDynDescription()
    {
        $parser = dyn(fn() => null);
        $this->assertSame("A dynamically parsed input", $parser->getDescription());
        $parser = integer()->dyn(fn() => null);
        $this->assertSame("A dynamically parsed input, based on one integer", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            [
                '1234Z',
                many0(
                    digit() /* <== Here, "digit()" is useless by intention */
                        ->dyn(function(StreamInterface $stream): ParseResult {
                            $takeResult = $stream->takeOne();
                            if ($takeResult === null) {
                                return ParseResult::error($stream);
                            }

                            $token = $takeResult->token;

                            $output = match ($token) {
                                '1' => 'One',
                                '3' => 'Three',
                                default => is_numeric($token) ? $token : '!!!',
                            };

                            return ParseResult::success($takeResult->stream, $output);

                        }),
                )->toString(),
                'One2Three4!!!',
                5,
            ],
            [
                '1234Z',
                many0(
                    digit()
                        ->dyn(function(StreamInterface $stream, ParserInterface $digit): ParseResult {
                            $parseResult = $digit->parse($stream);
                            if (!$parseResult->isSuccess()) {
                                return $parseResult;
                            }

                            $output = $parseResult->output;
                            $output = match ($output) {
                                '1' => 'One',
                                '3' => 'Three',
                                default => is_numeric($output) ? $output : '!!!',
                            };

                            return ParseResult::success($parseResult->stream, $output);
                        }),
                )->toString(),
                'One2Three4',
                4,
            ],
        ];
    }
}
