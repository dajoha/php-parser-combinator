<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Dynamic;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Dynamic\DynParser;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Dynamic\dynParser;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(DynParser::class)]
class DynParserTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testDynParser(
        StreamInterface|string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
    }

    public static function dataProvider(): array
    {
        return [
            [
                '1234',
                many0(
                    dynParser(function(StreamInterface $stream): ParserInterface {
                        if ((int) $stream->takeOne()?->token <= 2) {
                            return digit()->map(fn($c) => "$c$c");
                        } else {
                            return digit();
                        }
                    })
                )->toString(),
                '112234',
            ],
            [
                new StringStream('12!34', 'SINGLE'),
                many0(
                    dynParser(function(StreamInterface $stream): ParserInterface {
                        if ($stream->getContext() === 'DOUBLE') {
                            return any()->map(fn($c) => "$c$c");
                        } else {
                            return any();
                        }
                    })
                    ->alterContext(function(ParseResult $parseResult) {
                        if ($parseResult->output === '!') {
                            return 'DOUBLE';
                        } else {
                            return $parseResult->stream->getContext();
                        }
                    })
                )->toString(),
                '12!3344',
            ],
        ];
    }
}
