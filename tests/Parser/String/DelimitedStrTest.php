<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Parser\String\DelimitedStr;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\delimitedStr;

#[CoversClass(DelimitedStr::class)]
class DelimitedStrTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testDelimitedStr(
        string $input,
        string $startChar,
        ?string $endChar,
        ?string $escapeChar,
        ?string $expectedOutput,
        int $expectedResultCharPosition,
    ) {
        $parseResult = delimitedStr($startChar, $endChar, $escapeChar)->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertEquals($expectedResultCharPosition, $stream->getPosition()->charPosition);
    }

    public static function dataProvider(): array
    {
        return [
            ['', '', null, null, '', 0],
            ['', '"', null, null, null, 0],
            ['', '"', '"', null, null, 0],
            ['"', '"', '"', null, null, 1],
            ['"abc', '"', '"', null, null, 4],
            ['""', '"', '"', null, '', 2],
            ['""', '"', null, null, '', 2],
            ['"\\"', '"', null, null, '\\', 3],
            ['"abc"', '"', '"', null, 'abc', 5],
            ['"\\"', '"', null, '\\', null, 3],
            ['"\\A"', '"', null, '\\', 'A', 4],
            ['"\\\\"', '"', null, '\\', '\\', 4],
            ['":', '"', ':', null, '', 2],
            ['"\\:', '"', ':', null, '\\', 3],
            ['"abc:', '"', ':', null, 'abc', 5],
            ['"\\:', '"', ':', '\\', null, 3],
            ['"\\A:', '"', ':', '\\', 'A', 4],
            ['"\\\\:', '"', ':', '\\', '\\', 4],
            ['@\\\\\\::', '@', ':', '\\', '\\:', 6],
        ];
    }
}
