<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\String\IStr;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\iStr;

#[CoversClass(IStr::class)]
class IStrTest extends TestCase
{
    /**
     * @param array<array{string,ParseStatus}> $parses
     */
    #[DataProvider('dataProvider')]
    public function testParsingStringCaseInsensitive(string $input, array $parses)
    {
        $stream = new StringStream($input);

        foreach ($parses as $parse) {
            [$string, $expectedStatus] = $parse;
            $parseResult = iStr($string)->parse($stream);
            $this->assertSame($expectedStatus, $parseResult->status);
            $stream = $parseResult->stream;
        }
    }

    public function testIStrDescription()
    {
        $this->assertSame('"hi" (case-insensitive)', iStr('hi')->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            [
                '',
                [
                    ['', ParseStatus::Success],
                    ['', ParseStatus::Success],
                ],
            ], [
                'abc',
                [
                    ['', ParseStatus::Success],
                    ['', ParseStatus::Success],
                ],
            ], [
                '',
                [
                    ['a', ParseStatus::Error],
                ],
            ], [
                '',
                [
                    ['abc', ParseStatus::Error],
                ],
            ], [
                'abc',
                [
                    ['a', ParseStatus::Success],
                    ['B', ParseStatus::Success],
                    ['a', ParseStatus::Error],
                ],
            ], [
                'abc',
                [
                    ['ab', ParseStatus::Success],
                    ['c', ParseStatus::Success],
                ],
            ], [
                'abc',
                [
                    ['aB', ParseStatus::Success],
                    ['C', ParseStatus::Success],
                ],
            ], [
                'abc',
                [
                    ['ABc', ParseStatus::Success],
                    ['', ParseStatus::Success],
                    ['a', ParseStatus::Error],
                ],
            ], [
                'abc',
                [
                    ['abd', ParseStatus::Error],
                ],
            ], [
                'abc',
                [
                    ['abcd', ParseStatus::Error],
                ],
            ],
        ];
    }
}
