<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\FakeStructure;

enum FakeNum: int
{
    case Pi = 314;
    case Pi5Digits = 31415;
    case Gold = 1618;
}
