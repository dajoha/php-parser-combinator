<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\FakeStructure;

enum FakeColor: string
{
    case Red = 'R';
    case Green = 'G';
    case Blue = 'B';
}
