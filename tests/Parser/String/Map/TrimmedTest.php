<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\Map;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\String\Map\ToString;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Token\any;

#[CoversClass(ToString::class)]
class TrimmedTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testTrimmed(string $input, ParserInterface $parser, string $expectedOutput)
    {
        $this->assertSame($expectedOutput, $parser->trimmed()->parse($input)->output);
    }

    public function testTrimmedDescription()
    {
        $parser = many0(any())->trimmed();
        $this->assertSame("0-or-more of any token (trimmed)", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', many0(any()), ''],
            ["  \n\t ", many0(any()), ''],
            ["\n123abc", many0(any()), '123abc'],
            ["\n123abc   ", many0(any()), '123abc'],
        ];
    }
}
