<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\Map;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\String\Map\ToInt;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Multi\separated1;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\spaces0;

#[CoversClass(ToInt::class)]
class ToIntTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testToInt(
        string $input,
        ParserInterface $parser,
        ?int $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->toInt()->parse($input);

        $this->assertSame($expectedOutput, $parseResult->output);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public static function dataProvider(): array
    {
        return [
            ['', many0(digit()), null, 0],
            ['0...', many0(digit()), 0, 1],
            ['123abc', many0(digit()), 123, 3],
            ['1 2   3', separated1(digit(), spaces0()), 123, 7],
            ['123abc  ', seq(many0(digit()), identifier()), 123, 6],
        ];
    }
}
