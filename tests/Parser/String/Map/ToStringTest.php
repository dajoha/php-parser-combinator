<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\Map;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\String\Map\ToString;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Character\digit;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\identifier;

#[CoversClass(ToString::class)]
class ToStringTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testToString(string $input, ParserInterface $parser, string $expectedOutput)
    {
        $this->assertSame($expectedOutput, $parser->toString()->parse($input)->output);
    }

    public function testToStringDescription()
    {
        $parser = many0(digit())->toString();
        $this->assertSame("0-or-more of one digit converted to a string", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', many0(digit()), ''],
            ['123abc', many0(digit()), '123'],
            ['123abc  ', seq(many0(digit()), identifier()), '123abc'],
        ];
    }
}
