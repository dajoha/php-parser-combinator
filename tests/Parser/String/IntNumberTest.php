<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Parser\String\IntNumber;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(IntNumber::class)]
class IntNumberTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testParsingInteger(string $string, int $expected): void
    {
        $this->assertSame($expected, integer()->parse($string)->output);
    }

    #[DataProvider('failingDataProvider')]
    public function testParsingIntegerFails(string $string): void
    {
        $this->assertTrue(integer()->parse($string)->isError());
    }

    public static function dataProvider(): array
    {
        return [
            ['0', 0],
            ['123', 123],
            ['0012ab', 12],
            ['-123', -123],
        ];
    }

    public static function failingDataProvider(): array
    {
        return [
            [''],
            ['abc'],
        ];
    }
}
