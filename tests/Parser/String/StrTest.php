<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\String\Str;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\str;

#[CoversClass(Str::class)]
class StrTest extends TestCase
{
    /**
     * @param array<array{string,ParseStatus}> $parses
     */
    #[DataProvider('dataProvider')]
    public function testParsingString(string $input, array $parses)
    {
        $stream = new StringStream($input);

        foreach ($parses as $parse) {
            [$string, $expectedStatus] = $parse;
            $parseResult = str($string)->parse($stream);
            $this->assertSame($expectedStatus, $parseResult->status);
            $stream = $parseResult->stream;
        }
    }

    public function testStrDescription()
    {
        $this->assertSame('"hi"', str('hi')->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            [
                '',
                [
                    ['', ParseStatus::Success],
                    ['', ParseStatus::Success],
                ],
            ], [
                'abc',
                [
                    ['', ParseStatus::Success],
                    ['', ParseStatus::Success],
                ],
            ], [
                '',
                [
                    ['a', ParseStatus::Error],
                ],
            ], [
                '',
                [
                    ['abc', ParseStatus::Error],
                ],
            ], [
                'abc',
                [
                    ['a', ParseStatus::Success],
                    ['a', ParseStatus::Error],
                ],
            ], [
                'abc',
                [
                    ['ab', ParseStatus::Success],
                    ['c', ParseStatus::Success],
                ],
            ], [
                'abc',
                [
                    ['abc', ParseStatus::Success],
                    ['', ParseStatus::Success],
                    ['a', ParseStatus::Error],
                ],
            ], [
                'abc',
                [
                    ['abd', ParseStatus::Error],
                ],
            ], [
                'abc',
                [
                    ['abcd', ParseStatus::Error],
                ],
            ],
        ];
    }
}
