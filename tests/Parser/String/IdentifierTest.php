<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Parser\String\Identifier;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\identifier;

#[CoversClass(Identifier::class)]
class IdentifierTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testParsingIdentifier(string $string, string $expected): void
    {
        $this->assertSame($expected, identifier()->parse($string)->output);
    }

    #[DataProvider('failingDataProvider')]
    public function testParsingIdentifierFails(string $string): void
    {
        $this->assertTrue(identifier()->parse($string)->isError());
    }

    public static function dataProvider(): array
    {
        return [
            ['a', 'a'],
            ['a!!', 'a'],
            ['_', '_'],
            ['_...', '_'],
            ['a1', 'a1'],
            ['a1..', 'a1'],
            ['abc_12_%', 'abc_12_'],
            ['_12_%', '_12_'],
        ];
    }

    public static function failingDataProvider(): array
    {
        return [
            [''],
            ['1'],
            ['..'],
        ];
    }
}
