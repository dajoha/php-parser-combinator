<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Helper\Predicate as P;
use Dajoha\ParserCombinator\Parser\String\AnyChar1;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\anyChar1;

#[CoversClass(AnyChar1::class)]
class AnyChar1Test extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testAnyChar1(string $input, ParserInterface $parser, ?string $expectedOutput)
    {
        $parseResult = $parser->parse($input);
        if ($expectedOutput === null) {
            $this->assertFalse($parseResult->isSuccess());
        } else {
            $this->assertEquals($expectedOutput, $parseResult->output);
        }
    }

    public static function dataProvider(): array
    {
        return [
            [
                '',
                anyChar1(P::isLower()),
                null,
            ],
            [
                '321',
                anyChar1(P::isLower()),
                null,
            ],
            [
                'abc321',
                anyChar1(P::isLower()),
                'abc',
            ],
        ];
    }
}
