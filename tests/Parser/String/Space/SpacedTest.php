<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\Space;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\String\Space\Spaced;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\String\spaced;

#[CoversClass(Spaced::class)]
class SpacedTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testSpaced(
        string $input,
        ParserInterface $innerParser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parser = spaced($innerParser);
        $parseResult = $parser->parse($input);

        $this->assertSame($expectedOutput, $parseResult->output);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testSpacedDescription()
    {
        $parser = integer()->spaced();
        $this->assertSame("One integer (spaced)", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', integer(), null, 0],
            ['  ', integer(), null, 2],
            ['12', integer(), 12, 2],
            ['  12', integer(), 12, 4],
            ['12   ', integer(), 12, 5],
            ['   12   ', integer(), 12, 8],
        ];
    }
}
