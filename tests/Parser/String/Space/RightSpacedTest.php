<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\Space;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\String\Space\RightSpaced;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\String\rightSpaced;

#[CoversClass(RightSpaced::class)]
class RightSpacedTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testRightSpaced(
        string $input,
        ParserInterface $innerParser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parser = rightSpaced($innerParser);
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);

        $this->assertSame($expectedOutput, $parseResult->output);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testRightSpacedDescription()
    {
        $parser = integer()->rightSpaced();
        $this->assertSame("One integer (right-spaced)", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', integer(), null, 0],
            ['  ', integer(), null, 0],
            ['  12', integer(), null, 0],
            ['12', integer(), 12, 2],
            ['12   ', integer(), 12, 5],
        ];
    }
}
