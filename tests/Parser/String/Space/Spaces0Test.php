<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\Space;

use Dajoha\ParserCombinator\Parser\Sequence\OutputWrapper\IgnoredOutput;
use Dajoha\ParserCombinator\Parser\String\Space\Spaces0;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\spaces0;

#[CoversClass(Spaces0::class)]
class Spaces0Test extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testSpaces0(string $input, mixed $expectedOutput, int $expectedResultIndex)
    {
        $parseResult = spaces0()
            ->map(fn(IgnoredOutput $ignoredOutput) => implode($ignoredOutput->output))
            ->parse($input)
        ;

        $this->assertSame($expectedOutput, $parseResult->output);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public static function dataProvider(): array
    {
        return [
            ['', '', 0],
            ['abc', '', 0],
            [' ', ' ', 1],
            ['   ', '   ', 3],
            [' abc', ' ', 1],
            ["\n", "\n", 1],
            ["\n   ", "\n   ", 4],
            ["\nabc   ", "\n", 1],
            ["\r", "\r", 1],
            ["\r   ", "\r   ", 4],
            ["\rabc   ", "\r", 1],
            ["\r\n", "\r\n", 2],
            ["\r\n   ", "\r\n   ", 5],
            ["\r\nabc   ", "\r\n", 2],
            ["\t\t\tfoo", "\t\t\t", 3],
            ["\r\n   \t\r\n!", "\r\n   \t\r\n", 8],
        ];
    }
}
