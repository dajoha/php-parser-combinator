<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\Space;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\String\Space\LeftSpaced;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\String\leftSpaced;

#[CoversClass(LeftSpaced::class)]
class LeftSpacedTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testLeftSpaced(
        string $input,
        ParserInterface $innerParser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {

        $parser = leftSpaced($innerParser);
        $parseResult = $parser->parse($input);

        $this->assertSame($expectedOutput, $parseResult->output);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testLeftSpacedDescription()
    {
        $parser = integer()->leftSpaced();
        $this->assertSame("One integer (left-spaced)", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', integer(), null, 0],
            ['  ', integer(), null, 2],
            ['12', integer(), 12, 2],
            ['   12', integer(), 12, 5],
            ['   12   ', integer(), 12, 5],
        ];
    }
}
