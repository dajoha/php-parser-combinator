<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String\Space;

use Dajoha\ParserCombinator\Parser\String\Space\Space;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\space;

#[CoversClass(Space::class)]
class SpaceTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testSpace(string $input, mixed $expectedOutput, int $expectedResultIndex)
    {
        $parseResult = space()->parse($input);

        $this->assertSame($expectedOutput, $parseResult->output);

        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public static function dataProvider(): array
    {
        return [
            ['', null, 0],
            ['abc', null, 0],
            [' ', ' ', 1],
            ['   ', ' ', 1],
            [' abc', ' ', 1],
            ["\n", "\n", 1],
            ["\n   ", "\n", 1],
            ["\nabc   ", "\n", 1],
            ["\r", "\r", 1],
            ["\r   ", "\r", 1],
            ["\rabc   ", "\r", 1],
            ["\r\n", "\r\n", 2],
            ["\r\n   ", "\r\n", 2],
            ["\r\nabc   ", "\r\n", 2],
        ];
    }
}
