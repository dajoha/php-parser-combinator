<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Helper\Predicate as P;
use Dajoha\ParserCombinator\Parser\String\AnyChar0;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\anyChar0;

#[CoversClass(AnyChar0::class)]
class AnyChar0Test extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testAnyChar0(string $input, ParserInterface $parser, ?string $expectedOutput)
    {
        $parseResult = $parser->parse($input);
        if ($expectedOutput === null) {
            $this->assertFalse($parseResult->isSuccess());
        } else {
            $this->assertEquals($expectedOutput, $parseResult->output);
        }
    }

    public static function dataProvider(): array
    {
        return [
            [
                '',
                anyChar0(P::isLower()),
                '',
            ],
            [
                '321',
                anyChar0(P::isLower()),
                '',
            ],
            [
                'abc321',
                anyChar0(P::isLower()),
                'abc',
            ],
        ];
    }
}
