<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Parser\String\FloatNumber;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\float;

#[CoversClass(FloatNumber::class)]
class FloatNumberTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testParsingFloat(
        string $string,
        float $expectedOutput,
        int $expectedResultIndex,
    ): void
    {
        $parseResult = float()->parse($string);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    #[DataProvider('failingDataProvider')]
    public function testParsingFloatFails(string $string): void
    {
        $this->assertTrue(float()->parse($string)->isError());
    }

    public static function dataProvider(): array
    {
        return [
            ['0'        , 0.0   , 1],
            ['0.0'      , 0.0   , 3],
            ['00.000'   , 0.0   , 6],
            ['123'      , 123.0 , 3],
            ['123.00'   , 123.0 , 6],
            ['0012ab'   , 12.0  , 4],
            ['0012.00ab', 12.0  , 7],
            ['-123'     , -123.0, 4],
            ['-123.0'   , -123.0, 6],
            ['0.1'      , 0.1   , 3],
            ['-0.1'     , -0.1  , 4],
            ['123.1'    , 123.1 , 5],
            ['-123.1'   , -123.1, 6],
            ['-123.100a', -123.1, 8],
        ];
    }

    public static function failingDataProvider(): array
    {
        return [
            [''],
            ['abc'],
            ['.'],
            ['0.'],
            ['.12'],
        ];
    }
}
