<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\String\BackedEnum;
use Dajoha\ParserCombinator\Tests\Parser\String\FakeStructure\FakeColor;
use Dajoha\ParserCombinator\Tests\Parser\String\FakeStructure\FakeNum;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Multi\separated0;
use function Dajoha\ParserCombinator\Parser\String\backedEnum;
use function Dajoha\ParserCombinator\Parser\String\spaced;

#[CoversClass(BackedEnum::class)]
class BackedEnumTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testBackedEnum(string $input, ParserInterface $parser, mixed $expectedOutput)
    {
        $parseResult = $parser->parse($input);
        $this->assertEquals($expectedOutput, $parseResult->output);
    }

    /**
     * @throws ReflectionException
     */
    public static function dataProvider(): array
    {
        $colorParser = backedEnum(FakeColor::class);
        $numParser = backedEnum(FakeNum::class);

        return [
            [ '', $colorParser, null ],
            [ 'a', $colorParser, null ],
            [ 'r', $colorParser, null ],
            [ 'R', $colorParser, FakeColor::Red ],
            [ 'B', $colorParser, FakeColor::Blue ],
            [
                'RRBG',
                many0($colorParser),
                [ FakeColor::Red, FakeColor::Red, FakeColor::Blue, FakeColor::Green ],
            ],
            [
                '3141618',
                many0($numParser),
                [ FakeNum::Pi, FakeNum::Gold ],
            ],
            [
                '1618314',
                many0($numParser),
                [ FakeNum::Gold, FakeNum::Pi ],
            ],
            [
                '314153141618',
                many0($numParser),
                [ FakeNum::Pi5Digits, FakeNum::Pi, FakeNum::Gold ],
            ],
            [
                '314, 1618',
                separated0($numParser, spaced(',')),
                [ FakeNum::Pi, FakeNum::Gold ],
            ],
        ];
    }
}
