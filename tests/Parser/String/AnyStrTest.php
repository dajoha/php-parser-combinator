<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\String;

use Dajoha\ParserCombinator\Parser\String\AnyStr;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\anyStr;

#[CoversClass(AnyStr::class)]
class AnyStrTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testAnyStr(
        string $input,
        array|string $forbiddenChars,
        ?string $escapeChar,
        string $expectedOutput,
        int $expectedResultCharPosition,
    ) {
        $parseResult = anyStr($forbiddenChars, $escapeChar)->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertEquals($expectedResultCharPosition, $stream->getPosition()->charPosition);
    }

    public function testAnyStrDescription()
    {
        $parser = anyStr();
        $this->assertSame("A custom string", $parser->getDescription());
        $parser = anyStr('-=');
        $this->assertSame("A custom string (forbidden chars: '-', '=')", $parser->getDescription());
        $parser = anyStr('-=', '\\');
        $this->assertSame("A custom string (forbidden chars: '-', '=', escape char: '\\')", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', '', null, '', 0],
            ['', [], null, '', 0],
            ['===', '=', null, '', 0],
            ['===', ['='], null, '', 0],
            ['abc', '', null, 'abc', 3],
            ['abc==', '', null, 'abc==', 5],
            ['abc==', '=', null, 'abc', 3],
            ['abc\\=def', '=', '\\', 'abc=def', 8],
            ['abc\\=def\\\\ab\\c', '=', '\\', 'abc=def\\abc', 14],
            ['abc=', '=:', null, 'abc', 3],
            ['abc:', '=:', null, 'abc', 3],
        ];
    }
}
