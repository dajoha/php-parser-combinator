<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Combinator;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\Combinator\Not;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Combinator\not;
use function Dajoha\ParserCombinator\Parser\Misc\nothing;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Not::class)]
class NotTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testNot(
        string $input,
        ParserInterface $parser,
        ParseStatus $expectedStatus,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
        $this->assertSame($expectedStatus, $parseResult->status);
    }

    public function testNotDescription()
    {
        $parser = not(integer());
        $this->assertSame("Not one integer", $parser->getDescription());
    }

    public static function dataProvider(): array {
        return [
            ['', not(integer()), ParseStatus::Success, null, 0],
            ['abc', not(integer()), ParseStatus::Success, null, 0],
            ['', not(nothing()), ParseStatus::Error, null, 0],
            ['abc', not(nothing()), ParseStatus::Error, null, 0],
            ['123', not(integer()), ParseStatus::Error, null, 3],
            ['123ab', not(integer()), ParseStatus::Error, null, 3],
            ['123ab', not([integer(), identifier()]), ParseStatus::Error, null, 5],
        ];
    }
}
