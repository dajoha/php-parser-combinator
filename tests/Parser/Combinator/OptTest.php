<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Combinator;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Combinator\Opt;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Combinator\opt;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Opt::class)]
class OptTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testOpt(
        string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
        $this->assertTrue($parseResult->isSuccess());
    }

    public function testOptDescription()
    {
        $parser = opt(alt(integer(), identifier()));
        $this->assertSame("An optional match of a choice between one integer or one identifier", $parser->getDescription());
    }

    public static function dataProvider(): array {
        return [
            ['', opt(integer()), null, 0],
            ['123', opt(integer()), 123, 3],
            ['123!', opt(integer()), 123, 3],
            ['123abc', seq(opt(integer()), identifier()), [123, 'abc'], 6],
            ['abc', seq(opt(integer()), identifier()), [null, 'abc'], 3],
        ];
    }
}
