<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Combinator;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Parser\Debug\Debug;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Multi\separated1;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\String\spaced;

#[CoversClass(Debug::class)]
class DebugTest extends TestCase
{
    public function testDebug()
    {
        $input = '4, 10, 300';
        $sum = 0;
        $callback = function(ParseResult $parseResult) use (&$sum) {
            $sum += $parseResult->output;
        };
        $parser = separated1(integer()->debug($callback), spaced(','));
        $parser->parse($input);
        $this->assertSame(314, $sum);
    }

    public function testDebugDescription()
    {
        $parser = integer()->debug(fn() => null);
        $this->assertSame("One integer with debug callback", $parser->getDescription());
    }
}
