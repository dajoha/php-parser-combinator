<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Combinator;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Parser\Combinator\OrFatal;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(OrFatal::class)]
class OrFatalTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testOrFatal(
        string $input,
        ParserInterface $parser,
        ParseStatus $expectedParserStatus,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
        $this->assertSame($expectedParserStatus, $parseResult->status);
    }

    public function testOrFatalDescription()
    {
        $parser = integer()->orFatal();
        $this->assertSame("A match of one integer, or fatal", $parser->getDescription());
    }

    public static function dataProvider(): array
    {
        return [
            ['', integer(), ParseStatus::Error, null, 0],
            ['', integer()->orFatal(), ParseStatus::Fatal, null, 0],
            ['123', integer()->orFatal(), ParseStatus::Success, 123, 3],
        ];
    }
}
