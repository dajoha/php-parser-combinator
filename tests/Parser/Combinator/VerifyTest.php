<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Combinator;

use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Combinator\Verify;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Multi\separated0;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\String\spaced;

#[CoversClass(Verify::class)]
class VerifyTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testVerify(
        StreamInterface|string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testVerifyDescription()
    {
        $parser = integer()->verify(fn($n) => null);
        $this->assertSame("One integer which verifies a predicate", $parser->getDescription());
    }

    public static function dataProvider(): array {
        $isSmall = fn($n) => $n < 10;

        return [
            ['', integer()->verify($isSmall), null, 0],
            ['100', integer()->verify($isSmall), null, 3],
            ['7', integer()->verify($isSmall), 7, 1],
            ['7!', integer()->verify($isSmall), 7, 1],
            [
                new StringStream('2, 45000, 33, 666, 33, 41000, 12', false),
                separated0(
                    integer()
                        ->alterContext(function(ParseResult $r) {
                            return $r->output === 666 ? 'LESS_THAN_40000' : $r->getContext();
                        })
                        ->verify(function($n, ParseResult $r) {
                            return $r->getContext() !== 'LESS_THAN_40000' || $n < 40000;
                        })
                    ,
                    spaced(','),
                ),
                [2, 45000, 33, 666, 33],
                21,
            ],
        ];
    }
}
