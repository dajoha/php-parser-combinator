<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Parser\Combinator;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Parser\Combinator\Alt;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\integer;

#[CoversClass(Alt::class)]
class AltTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testAlt(
        string $input,
        ParserInterface $parser,
        mixed $expectedOutput,
        int $expectedResultIndex,
    ) {
        $parseResult = $parser->parse($input);
        $this->assertSame($expectedOutput, $parseResult->output);
        /** @var StringStream $stream */
        $stream = $parseResult->stream;
        $this->assertSame($expectedResultIndex, $stream->index);
    }

    public function testAltDescription()
    {
        $parser = alt(integer(), identifier());
        $this->assertSame("A choice between one integer or one identifier", $parser->getDescription());
        $parser = alt(integer(), identifier(), identifier(), identifier());
        $this->assertSame(
            <<< EOF
            A choice between:
             - One integer
             - One identifier
             - One identifier
             - One identifier

            EOF,
            $parser->getDescription(),
        );
    }

    public static function dataProvider(): array {
        return [
            ['', alt(), null, 0],
            ['', alt(integer(), identifier()), null, 0],
            ['!!!', alt(integer(), identifier()), null, 0],
            ['7!!!', alt(integer(), identifier()), 7, 1],
            ['123!!!', alt(integer(), identifier()), 123, 3],
            ['abc!!!', alt(integer(), identifier()), 'abc', 3],
        ];
    }
}
