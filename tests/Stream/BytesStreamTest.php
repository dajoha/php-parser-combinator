<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Stream;

use Dajoha\ParserCombinator\Stream\BytesStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(BytesStream::class)]
class BytesStreamTest extends TestCase
{
    #[TestDox('BytesStream with input "$input"')]
    #[DataProvider('dataProvider')]
    function testBytesStream(string $input, int $len, array $chars) {
        $stream = new BytesStream($input);
        $index = 0;

        $this->assertStreamEquals($stream, $input, $len, $index, empty($chars));

        foreach ($chars as $index => $char) {
            $takeResult = $stream->takeOne();
            $this->assertSame($char, $takeResult->token);
            /** @var BytesStream $stream */
            $stream = $takeResult->stream;
            $index++;
            $this->assertStreamEquals($stream, $input, $len, $index, $len === $index);
        }

        $this->assertTrue($stream->isEof());
        $this->assertNull($stream->takeOne());
    }

    protected function assertStreamEquals(
        BytesStream $stream,
        string $input,
        int $len,
        int $index,
        ?bool $isEof = null,
    ): void {
        $this->assertSame($input, $stream->input);
        $this->assertSame($len, $stream->len);
        $this->assertSame($index, $stream->index);
        $this->assertSame($index, $stream->getPosition());
        if ($isEof !== null) {
            $this->assertSame($isEof, $stream->isEof());
        }
    }

    public static function dataProvider(): array
    {
        return [
            ['', 0, []],
            ['h', 1, ['h']],
            ['héi', 4, ['h', chr(195), chr(169), 'i']],
        ];
    }
}
