<?php

/** @noinspection PhpIdempotentOperationInspection */

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Stream;

use Dajoha\ParserCombinator\Stream\LocatedStream\LocatedStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(LocatedStream::class)]
class LocatedStreamTest extends TestCase
{
    /**
     * @param array<array{int, int, ?string}> $takes
     */
    #[TestDox('LocatedStream with $baseLine-based lines and $baseColumn-based columns')]
    #[DataProvider('dataProvider')]
    public function testLocatedStream(string $input, int $baseLine, int $baseColumn, array $takes)
    {
        $stream = new LocatedStream($input, $baseLine, $baseColumn);

        foreach ($takes as $take) {
            [$line, $column, $char] = $take;
            $this->assertSame($line, $stream->line);
            $this->assertSame($column, $stream->column);
            $takeResult = $stream->takeOne();
            if ($char === null) {
                $this->assertNull($takeResult);
            } else {
                $this->assertSame($char, $takeResult->token);
                $stream = $takeResult->stream;
            }
        }
    }

    public static function dataProvider(): array
    {
        $basePositions = [
            [0, 0],
            [1, 1],
        ];

        $data = [];

        foreach ($basePositions as $basePosition) {
            [$baseLine, $baseColumn] = $basePosition;

            $data = array_merge($data, [
                ['', $baseLine, $baseColumn, [[$baseLine, $baseColumn, null]]],
                [
                    <<< EOF
                    ab
                    cd
                    EOF,
                    $baseLine,
                    $baseColumn,
                    [
                        [$baseLine + 0, $baseColumn + 0, 'a'],
                        [$baseLine + 0, $baseColumn + 1, 'b'],
                        [$baseLine + 0, $baseColumn + 2, "\n"],
                        [$baseLine + 1, $baseColumn + 0, 'c'],
                        [$baseLine + 1, $baseColumn + 1, 'd'],
                        [$baseLine + 1, $baseColumn + 2, null],
                    ],
                ], [
                    <<< EOF
                    éb
                    cd
                    EOF,
                    $baseLine,
                    $baseColumn,
                    [
                        [$baseLine + 0, $baseColumn + 0, 'é'],
                        [$baseLine + 0, $baseColumn + 1, 'b'],
                        [$baseLine + 0, $baseColumn + 2, "\n"],
                        [$baseLine + 1, $baseColumn + 0, 'c'],
                        [$baseLine + 1, $baseColumn + 1, 'd'],
                        [$baseLine + 1, $baseColumn + 2, null],
                    ],
                ],
            ]);
        }

        return $data;
    }
}
