<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Stream;

use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(StringStream::class)]
class StringStreamTest extends TestCase
{
    #[TestDox('StringStream with input "$input"')]
    #[DataProvider('dataProvider')]
    function testStringStream(string $input, int $len, array $chars) {
        $stream = new StringStream($input);
        $index = 0;
        $position = 0;

        $this->assertStreamEquals($stream, $input, $len, 0, 0, empty($chars));

        foreach ($chars as $char) {
            $takeResult = $stream->takeOne();
            $this->assertSame($takeResult->token, $char);
            /** @var StringStream $stream */
            $stream = $takeResult->stream;
            $index += strlen($takeResult->token);
            $position++;
            $this->assertStreamEquals($stream, $input, $len, $index, $position, $len === $index);
        }

        $this->assertTrue($stream->isEof());
        $this->assertNull($stream->takeOne());
    }

    protected function assertStreamEquals(
        StringStream $stream,
        string $input,
        int $len,
        int $index,
        int $position,
        ?bool $isEof = null,
    ): void {
        $this->assertEquals($input, $stream->input);
        $this->assertEquals($len, $stream->len);
        $this->assertEquals($index, $stream->index);
        $this->assertEquals($position, $stream->getPosition()->charPosition);
        if ($isEof !== null) {
            $this->assertEquals($isEof, $stream->isEof());
        }
    }

    public static function dataProvider(): array
    {
        return [
            ['', 0, []],
            ['h', 1, ['h']],
            ['hi', 2, ['h', 'i']],
            ['héi', 4, ['h', 'é', 'i']],
        ];
    }
}
