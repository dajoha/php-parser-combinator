<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Helper;

use Dajoha\ParserCombinator\Helper\Predicate;
use Dajoha\ParserCombinator\Helper\Predicate as P;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

#[CoversClass(Predicate::class)]
class PredicateTest extends TestCase
{
    /**
     * @noinspection PhpUnusedParameterInspection
     */
    #[DataProvider('dataProvider')]
    #[TestDox('Predicate $predicateName')]
    public function testPredicate(string $predicateName, callable $predicate, array $tests)
    {
        $negatedPredicate = P::not($predicate);

        foreach ($tests as $test) {
            [$character, $expectedResult] = $test;
            $this->assertEquals(
                $expectedResult,
                $predicate($character),
                "Character \"$character\"",
            );
            $this->assertEquals(
                !$expectedResult,
                $negatedPredicate($character),
                "Character \"$character\" (negated predicate)",
            );
        }
    }

    public static function dataProvider(): array
    {
        return [
            [
                'isDigit()',
                P::isDigit(),
                [
                    ['0', true],
                    ['1', true],
                    ['2', true],
                    ['8', true],
                    ['9', true],
                    ['a', false],
                    ["\x00", false],
                    ['ç', false],
                ],
            ],
            [
                'isAlpha()',
                P::isAlpha(),
                [
                    ['a', true],
                    ['b', true],
                    ['z', true],
                    ['A', true],
                    ['B', true],
                    ['Z', true],
                    ['1', false],
                    ['à', false],
                    ['ç', false],
                ],
            ],
            [
                'isLower()',
                P::isLower(),
                [
                    ['a', true],
                    ['b', true],
                    ['z', true],
                    ['A', false],
                    ['B', false],
                    ['Z', false],
                    ['1', false],
                    ['à', false],
                    ['ç', false],
                ],
            ],
            [
                'isUpper()',
                P::isUpper(),
                [
                    ['A', true],
                    ['B', true],
                    ['Z', true],
                    ['a', false],
                    ['b', false],
                    ['z', false],
                    ['1', false],
                    ['à', false],
                    ['ç', false],
                ],
            ],
            [
                'isAlphanumeric()',
                P::isAlphanumeric(),
                [
                    ['a', true],
                    ['b', true],
                    ['z', true],
                    ['A', true],
                    ['B', true],
                    ['Z', true],
                    ['0', true],
                    ['1', true],
                    ['2', true],
                    ['8', true],
                    ['9', true],
                    ["\x00", false],
                    ['ç', false],
                    ['à', false],
                ],
            ],
            [
                'isHSpace()',
                P::isHSpace(),
                [
                    [' ', true],
                    ["\t", true],
                    ['a', false],
                    ["\r", false],
                    ["\r\n", false],
                    ["\n", false],
                ],
            ],
            [
                'isVSpace()',
                P::isVSpace(),
                [
                    [' ', false],
                    ["\t", false],
                    ['a', false],
                    ["\r", true],
                    ["\r\n", true],
                    ["\n", true],
                ],
            ],
            [
                'isSpace()',
                P::isSpace(),
                [
                    [' ', true],
                    ["\t", true],
                    ["\r", true],
                    ["\r\n", true],
                    ["\n", true],
                    ['a', false],
                ],
            ],
            [
                'isPrintable()',
                P::isPrintable(),
                [
                    ['a', true],
                    [' ', true],
                    [':', true],
                    ["\x00", false],
                    ["\r", false],
                    ["\r\n", false],
                    ["\n", false],
                    ["\t", false],
                ],
            ],
            [
                'all()',
                P::all(P::isAlphanumeric(), P::not(P::isUpper())),
                [
                    ['a', true],
                    ['b', true],
                    ['z', true],
                    ['0', true],
                    ['1', true],
                    ['2', true],
                    ['8', true],
                    ['9', true],
                    ['A', false],
                    ['B', false],
                    ['Z', false],
                    [' ', false],
                    [':', false],
                ],
            ],
            [
                'any()',
                P::any('a', 'g', P::isDigit()),
                [
                    ['a', true],
                    ['g', true],
                    ['0', true],
                    ['1', true],
                    ['2', true],
                    ['8', true],
                    ['9', true],
                    ['b', false],
                    ['A', false],
                    ['B', false],
                    ['G', false],
                    [' ', false],
                    [':', false],
                ],
            ],
        ];
    }
}
