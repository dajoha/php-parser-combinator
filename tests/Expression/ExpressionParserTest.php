<?php

declare(strict_types=1);

namespace Dajoha\ParserCombinator\Tests\Expression;

use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Base\ParseStatus;
use Dajoha\ParserCombinator\Expression\Ast\BinaryOperation;
use Dajoha\ParserCombinator\Expression\Ast\UnaryOperation;
use Dajoha\ParserCombinator\Expression\ExpressionParser;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Combinator\not;
use function Dajoha\ParserCombinator\Parser\Misc\complete;
use function Dajoha\ParserCombinator\Parser\Misc\recursive;
use function Dajoha\ParserCombinator\Parser\Sequence\delimited;
use function Dajoha\ParserCombinator\Parser\String\delimitedStr;
use function Dajoha\ParserCombinator\Parser\String\float;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\String\spaced;
use function Dajoha\ParserCombinator\Parser\Token\oneOf;

#[CoversClass(ExpressionParser::class)]
class ExpressionParserTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testExpressionParser(string $input, mixed $expectedOutput)
    {
        $parseResult = self::getParser()->parse($input);
        $this->assertSame(
            $expectedOutput === null ? ParseStatus::Error : ParseStatus::Success,
            $parseResult->status,
        );
        $this->assertEquals($expectedOutput, $parseResult->output);
    }

    protected static function getParser(): ParserInterface
    {
        static $parser = null;

        if ($parser !== null) {
            return $parser;
        }

        $recursiveExpressionParser = recursive();

        $literal = alt(
            delimitedStr('"', escapeChar: '\\'),
            integer()->followedBy(not('.')),
            float(),
            identifier(),
        );

        $parser = ExpressionParser::precedenceParser(
            alt(
                $literal,
                delimited(
                    spaced('('),
                    $recursiveExpressionParser,
                    spaced(')'),
                ),
            ),
            [
                ExpressionParser::suffixOper(
                    spaced('++'),
                    fn($o, $v) => new UnaryOperation("Suffix $o", $v),
                ),
                ExpressionParser::prefixOper(
                    spaced('++'),
                    fn($o, $v) => new UnaryOperation("Prefix $o", $v),
                ),
                ExpressionParser::rightAssocBinaryOper(spaced('^')),
                ExpressionParser::leftAssocBinaryOper(spaced(oneOf('*/'))),
                ExpressionParser::leftAssocBinaryOper(spaced(oneOf('+-'))),
                ExpressionParser::rightAssocBinaryOper(spaced(alt('&&', '&'))),
                ExpressionParser::leftAssocBinaryOper(spaced(alt('||', '|'))),
            ],
        );

        $recursiveExpressionParser->setParser($parser);

        $parser = complete(spaced($parser));

        return $parser;
    }

    public static function dataProvider(): array
    {
        return [
            ['', null],
            ['1', 1],
            [' 1 ', 1],
            [' 1 2 ', null],
            [
                '1 + 2',
                new BinaryOperation('+', 1, 2),
            ], [
                '1 + 2 + 3',
                new BinaryOperation(
                    '+',
                    new BinaryOperation('+', 1, 2),
                    3,
                ),
            ], [
                '1 + 2 * 3 + 4 + 5',
                new BinaryOperation(
                    '+',
                    new BinaryOperation(
                        '+',
                        new BinaryOperation(
                            '+',
                            1,
                            new BinaryOperation('*', 2, 3),
                        ),
                        4,
                    ),
                    5,
                ),
            ], [
                '1 + "two" * (3 + 4) + 5',
                new BinaryOperation(
                    '+',
                    new BinaryOperation(
                        '+',
                        1,
                        new BinaryOperation(
                            '*',
                            "two",
                            new BinaryOperation('+', 3, 4),
                        ),
                    ),
                    5,
                ),
            ], [
                '1 + 777++ + 3',
                new BinaryOperation(
                    '+',
                    new BinaryOperation(
                    '+',
                        1,
                        new UnaryOperation('Suffix ++', 777),
                    ),
                    3,
                ),
            ], [
                '1 + ++777++ + 3',
                new BinaryOperation(
                    '+',
                    new BinaryOperation(
                    '+',
                        1,
                        new UnaryOperation(
                            'Prefix ++',
                            new UnaryOperation('Suffix ++', 777),
                        ),
                    ),
                    3,
                ),
            ],
        ];
    }
}
