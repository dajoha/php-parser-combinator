TODO list
=========

* Space functions: add an option in ctr to disable `Ignore`
* Add `->match(...)` to `ParserInterface`
* Add parser `Many0Verify`
* Add parser `Many1Verify`
* Parser `Not`: should be "ignored"
* Parser `Eol`: should be "ignored"
